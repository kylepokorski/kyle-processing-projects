class Square
{
  public int value;
  PVector position;
  float Size;
  float strokewidth;
  int numberofsquares;
  PFont font;
  public boolean showtile = false;
  public boolean iswall = false;
  public boolean trianglemode;
  public boolean facingup = true;
  public int row;
  public Square(int value, PVector position, float Size, float strokewidth, int numberofsquares, PFont font, boolean trianglemode, boolean facingup,int row)
  {
    this.value = value;
    this.position = position;
    this.strokewidth = strokewidth;
    this.numberofsquares = numberofsquares;
    this.font = font;
    this.Size = Size;
    this.trianglemode = trianglemode;
    this.facingup = facingup;
    this.row = row;
  }
  public void Draw()
  {
    if (value != numberofsquares- 1)
    {
      if (!iswall)
      {
        fill(0, 0, (value/(float)numberofsquares)*255);
      } else
      {
        fill(255, 0, 0);
      }
      strokeWeight(strokewidth);
      stroke(255, 255, 255);
      if (trianglemode)
      {
        if (facingup)
        {
          triangle(position.x, position.y, position.x + Size/2, position.y - Size, position.x + Size, position.y);
        } else
        {
          triangle(position.x, position.y-Size, position.x + Size/2, position.y, position.x + Size, position.y-Size);
        }
      } else
      {
        rect(position.x, position.y, Size, Size);
      }
      strokeWeight(1);
      fill(255, 255, 255);
      textFont(font);
      if (!trianglemode)
      {

        text(value+1, position.x, position.y + 20);
      } else
      {
        if (facingup)
        {
          text(value+1, position.x + Size/4, position.y + 20 -Size/2 );
        } else
        {
          text(value+1, position.x + Size/4, position.y - 20 -Size/2 );
        }
      }
    } else if (!showtile)
    {
      noFill();
      stroke(255, 255, 255);
      strokeWeight(strokewidth);
      if (trianglemode)
      {
        if (facingup)
        {
          triangle(position.x, position.y, position.x + Size/2, position.y - Size, position.x + Size, position.y);
        } else
        {
          triangle(position.x, position.y-Size, position.x + Size/2, position.y, position.x + Size, position.y-Size);
        }
      } else
      {
        rect(position.x, position.y, Size, Size);
      }
    } else
    {
      if (!iswall)
      {
        fill(0, 0, (value/(float)numberofsquares)*255);
      } else
      {
        fill(255, 0, 0);
      }
      strokeWeight(strokewidth);
      stroke(255, 255, 255);
      if (trianglemode)
      {
        if (facingup)
        {
          triangle(position.x, position.y, position.x + Size/2, position.y - Size, position.x + Size, position.y);
        } else
        {
          triangle(position.x, position.y-Size, position.x + Size/2, position.y, position.x + Size, position.y-Size);
        }
      } else
      {
        rect(position.x, position.y, Size, Size);
      }
      strokeWeight(1);
      fill(255, 255, 255);
      textFont(font);
      if (facingup)
      {
        text(value+1, position.x + Size/4, position.y + 20 -Size/2 );
      } else
      {
        text(value+1, position.x + Size/4, position.y - 20 -Size/2 );
      }
    }
  }
}
