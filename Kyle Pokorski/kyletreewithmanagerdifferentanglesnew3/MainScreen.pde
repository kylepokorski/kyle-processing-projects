public class MainScreen extends Screen
{
  public float _lengthdecrease = 2f;
  public float _lengthdecreaseright = 2f;
  float angleleft = 0;
  float angleright = 0;
  public float _anglechangeleft = 0.01f;
  public float _anglechangeright = 0.01f;
  public boolean threebranches = false;
  public int iterations = 10;
  boolean changewidth = true;
  PFont font;
  public boolean move = true;
  public int displayright = 0;
  public float _width = 10;

  public int _Length = 100;
  public int _LengthRight =100;
  public boolean circlemode = false;
  public int branches = 2;
  boolean differentbranchcolors = false;

  public color start =  color(100, 64, 0,255);
  public color end= color(0, 128, 0,255);
  color rightstart =  color(100, 64, 0,255);
  color rightend= color(0, 128, 0,255);
  public int symmetry = 1;
  boolean dosymmetry = true;
  public float startangleleft = 0;
  public float startangleright = 0;
  public  boolean compensate = true;
  public boolean seperateangleright =true;
  color textcolor = color(0, 0, 0);
  public boolean addnodemode = true;
  public float scale = 1f;
  boolean pluspressed = false;

  public boolean dualtree = true;
  int theme = 3;






  public void checklength()
  {
    if (_Length * (1-(float)pow(_lengthdecrease, iterations))/(float)(1-_lengthdecrease) * sin(iterations*angleleft) < 200)
    {
      _Length ++;
    } else
    {
      _Length--;
    }
    if (_LengthRight * (1-pow(_lengthdecrease, iterations))/(1-_lengthdecrease)  < 200)
    {
      _LengthRight ++;
    } else
    {
      _LengthRight--;
    }
  }
  public MainScreen(color backgroundcolor, String screenName)

  {
    super(backgroundcolor, screenName);
    font = loadFont("Arial-BoldMT-20.vlw");
  }
  void DualTree()
  {
    if (dualtree)
    {
      
        start = color(255-red(start), 255-green(start), 255-blue(start));
        rightstart = color(255-red(rightstart), 255-green(rightstart), 255-blue(rightstart));
        end = color(255-red(end), 255-green(end), 255-blue(end));
        rightend = color(255-red(rightend), 255-green(rightend), 255-blue(rightend));
      
      generate(new PVector(350, 500), _Length*scale*pow(_lengthdecrease, iterations-1), _LengthRight*scale*pow(_lengthdecrease, iterations-1), startangleleft + PI/2, startangleleft + PI/2, angleleft, angleright, 0, _width, 1/_lengthdecrease, 1/_lengthdecreaseright, 0);
    }
    start = color(255-red(start), 255-green(start), 255-blue(start));
    rightstart = color(255-red(rightstart), 255-green(rightstart), 255-blue(rightstart));
    end = color(255-red(end), 255-green(end), 255-blue(end));
    rightend = color(255-red(rightend), 255-green(rightend), 255-blue(rightend));
  }
  public void Draw()
  {

    textFont(font);
    super.Draw();
    if (!dosymmetry || symmetry == 1)
    {

      //generate(new PVector(350, 500), _Length, _LengthRight, startangleleft + PI/2, PI/2 -angleright, angleleft, angleright/60, 0, _width, _lengthdecrease, _lengthdecreaseright, 0);

      if (!seperateangleright)
      {
        if (_lengthdecrease <1 && dualtree)
        {
          DualTree();
        }
        generate(new PVector(350, 500), _Length*scale, _LengthRight*scale, startangleleft + PI/2, startangleleft + PI/2, angleleft, angleright, 0, _width, _lengthdecrease, _lengthdecreaseright, 0);
        if (_lengthdecrease >=1 && dualtree)
        {
          DualTree();
        }
      } else
      {
        generate(new PVector(350, 500), _Length*scale, _LengthRight*scale, startangleleft + PI/2, startangleright + PI/2, angleleft, angleright, 0, _width, _lengthdecrease, _lengthdecreaseright, 0);
      }
    } else
    {

      for (int i = 0; i<symmetry; i++)
      {
        if (seperateangleright)
        {
          generate(new PVector(350, 500), _Length*scale, _LengthRight*scale, 2*(float)i* (PI/(float)symmetry) + PI/2 +startangleleft, 2*(float)i* (PI/(float)symmetry) + PI/2+startangleright, angleleft, angleright, 0, _width, _lengthdecrease, _lengthdecreaseright, 0);
        } else
        {
          generate(new PVector(350, 500), _Length*scale, _LengthRight*scale, 2*(float)i* (PI/(float)symmetry) + PI/2 +startangleleft, 2*(float)i* (PI/(float)symmetry) + PI/2+startangleleft, angleleft, angleright, 0, _width, _lengthdecrease, _lengthdecreaseright, 0);
        }
      }
    }
    if (green(backgroundcolor) >=128 && blue(backgroundcolor) >=128 || green(backgroundcolor) >=128 && red(backgroundcolor) >=128)
    {
      textcolor = color(0, 0, 0);
    } else
    {
      textcolor = color(255, 255, 255);
    }
    if (angleleft < PI)
    {
    }
    if (move)
    {
      if (keyPressed)
      {
        if (keyCode =='=' && !pluspressed)
        {
          pluspressed = true;
        } 
        if (keyCode != '=')
        {
          pluspressed = false;
        }
        if (keyCode == '-')
        {
          if (scale >0.1)
          {
            scale-=0.01f;
          }
        }

        if (keyCode == SHIFT)
        {
          angleleft-=0.001f;
          angleright-=0.001f;
        }
        if (key == 'd')
        {
          startangleleft -= 0.001f;
        }
        if (key == 'a')
        {
          startangleleft += 0.001f;
        }
        if (keyCode == RIGHT)
        {
          startangleright -=0.01f;
        }
        if (keyCode == LEFT)
        {
          startangleright +=0.01f;
        }
      } else
      {
        angleleft+=_anglechangeleft;

        angleright+=_anglechangeright;
      }
    }
    if (pluspressed)
    {
      if (scale < 3)
      {
        scale = scale+ 0.01f;
      }
    }


    stroke(red(textcolor), green(textcolor), blue(textcolor));
    fill(255-red(textcolor), 255-green(textcolor), 255-blue(textcolor));
    strokeWeight(1);
    ellipse(400, 30, 60, 60);
    ellipse(600, 30, 60, 60);
    ellipse(25, 725, 30, 30);
    ellipse(110, 725, 30, 30);
    fill(textcolor);
    rect(595, 10, 5, 30);
    triangle(583, 30, 15 + 583, 30 + 20, 30+583, 30);
    strokeWeight(5);
    ellipse(400, 30, 25, 25);


    pushMatrix();
    translate(400, 30);

    for (int i = 0; i < 16; i++)
    {

      line(0, 0, 0, 20);
      rotate(PI/8);
    }

    popMatrix();
    fill(255f-red(textcolor), 255f-green(textcolor), 255f-blue(textcolor));
    line(15, 725, 35, 725);
    pushMatrix();
    translate(110, 725);
    line(-10, 0, 10, 0);
    rotate(PI/2);
    line(-10, 0, 10, 0);
    popMatrix();
    ellipse(400, 30, 15, 15);
    fill(textcolor);
    text((angleleft%(2*PI)/PI), 0, 20);
    text("pi rad", 60, 20);
    text((angleleft%(2*PI))/(PI/180), 120, 20);
    text("degrees", 200, 20);
    text((angleright%(2*PI)/PI), 0, 40);
    text("pi rad", 60, 40);
    text((angleright%(2*PI))/(PI/180), 120, 40);
    text("degrees", 200, 40);

    text(scale, 40, 735);


    if (theme == 0)
    {
      start = color(0, 0, 0);
      rightstart = color(0, 0, 0);
      rightend = color(0, 0, 255);
      end = color(0, 0, 255);
      backgroundcolor = color(190, 190, 190);
    }
    if (theme == 1)
    {
      start = color(100, 64, 0);
      end= color(0, 128, 0);
      rightstart = color(100, 64, 0);
      rightend= color(0, 128, 0);
      backgroundcolor = color(0, 191, 255);
    }
    if (theme == 3)
    {
      start = color(255, 255, 255);
      end= color(255, 255, 255);
      rightstart = color(255, 255, 255);
      rightend= color(255, 255, 255);
      backgroundcolor = color(0, 0, 0);
    }
    if (theme == 4)
    {
      start = color(255, 255, 255);
      rightstart = color(255, 255, 255);
      end = color(255, 255, 0);
      rightend = color(255, 255, 0);
      backgroundcolor = color(255-190, 255-190, 255-190);
    }
  }
  void generate(PVector point, float _length, float _lengthright, float angle, float _angleright, float anglechangeleft, float anglechangeright, int times, float strokewidth, float lengthdecreaseleft, float lengthdecreaseright, int branchnumber)
  {
    if (times <iterations)
    {
      if (iterations == 0)
      {
        _angleright = _angleright+anglechangeright;
      }
      strokeWeight(strokewidth);
      //stroke(0, 0, times *(255/iterations));
      color _color;
      color _rightcolor;
      _color = lerpColor(start, end, (float)times/(float)iterations);
      _rightcolor = lerpColor(rightstart, rightend, (float)times/(float)iterations);
      float newx;
      float newy;
      if (branchnumber == 0||branchnumber == 2)
      {
        stroke(_color);
        newx = point.x + cos(angle)*_length ;
        newy = point.y - _length *sin(angle);
      } else
      {
        //angle = _angleright;
        stroke(_rightcolor);
        newx = point.x + cos(_angleright)*_length ;
        newy = point.y - _length *sin(_angleright);
      }

      if (circlemode)
      {
        if (strokewidth <1)
        {
          strokewidth = 1;
        }



        ellipse(point.x, point.y, strokewidth, strokewidth);
      } else
      {
        if (times != 0)
        {
        }
        line(point.x, point.y, newx, newy);
        if (addnodemode)
        {
          ellipse(point.x, point.y, strokewidth, strokewidth);
        }
      }

      float newwidthleft;
      float newwidthright;
      if (changewidth)
      {
        newwidthleft = (float)(lengthdecreaseleft*strokewidth);
        newwidthright = (float)(lengthdecreaseright*strokewidth);
      } else
      {
        newwidthright = strokewidth;
        newwidthleft = strokewidth;
      }
      if (seperateangleright)
      {
        if (compensate)
        {
          generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft, _angleright-anglechangeright, anglechangeleft, anglechangeleft, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 0);
          if (times >= displayright && branches >= 2)
          {
            generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, _angleright + anglechangeright, angle+anglechangeleft, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 1);
          }
        } else
        {
          generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft, _angleright, anglechangeleft, anglechangeleft, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 0);
          if (times >= displayright && branches >= 2)
          {
            generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, _angleright + anglechangeright, angle, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 1);
          }
        }
        if (branches == 3 ||branches == 5 || branches == 7||branches == 9)
        {
          generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, PI/2-startangleleft, PI/2-startangleright, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 2);
        }
      } else
      {
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft, angle-anglechangeleft, anglechangeleft, anglechangeleft, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 0);
        if (times >= displayright && branches >= 2)
        {
          generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, _angleright + anglechangeright, _angleright+anglechangeright, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 1);
        }
        if (branches == 3 ||branches == 5 || branches == 7||branches == 9)
        {
          generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle, angle, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 2);
        }
        if (branches == 4||branches == 5)
        {
          generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + PI/2, angle + anglechangeright + PI/2, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 3);
          generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft -PI/2, angle - anglechangeleft -PI/2, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 4);
          //          generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright*2 , angle + anglechangeright*2 , anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 3);
          //generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft*2, angle - anglechangeleft*2, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 4);
        }

        if (branches == 6 || branches == 7)
        {
          generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + 2*PI/(branches), angle + anglechangeright + 2*PI/(branches), anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 3);
          generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + 4*PI/(branches), angle + anglechangeright + 4*PI/(branches), anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 4);

          generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - 2*PI/(branches), angle - anglechangeleft - 2*PI/(branches), anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 5);
          generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - 4*PI/(branches), angle - anglechangeleft - 4*PI/(branches), anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 6);
        }
        if (branches == 8||branches == 9)
        {
          // generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + PI/2, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 3);
          // generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft -PI/2, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 4);

          // generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright +  PI, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 3);
          // generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - PI, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 4);

          //generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright +  3*PI/2, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 5);
          //generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - 3*PI/2, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 6);
        }
      }
    }
  }
}
