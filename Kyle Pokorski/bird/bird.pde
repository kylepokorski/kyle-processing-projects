int frame = 1;
float angle = -PI/5;
float anglechange = -0.02f;
float posy= 12;
float speed = 1.4f;
void setup()
{
  size(500, 500);
  noStroke();
  frameRate(60);
}

void draw()
{
  background(0, 191, 255);
  noStroke();
  fill(0, 128, 0);
  rect(0, 300, 500, 200);
  if (angle > -PI/3)
  {
    angle +=anglechange;
  } else if (angle <0)
  {
    angle += anglechange;
  }

  if (angle < -PI/3 || angle > 0)
  {
    anglechange = anglechange*-1;
  }
  if (posy> 0||posy + 30 <= 300)
  {
    posy +=speed;
  }
  if (posy+ 30>300 || posy < 0)
  {
    speed*=-1;
  }
  pushMatrix();
  translate(180, 200);

  rotate(angle);
  fill(255, 64, 0);
  ellipse(0, -50, 50, 120);
  // ellipse(160,170,50,120);
  popMatrix();
  fill(0, 0, 255);
  rect(300, posy, 30, 30);
  rect(posy,300,30,30);
  fill(255, 255, 0);
  circle(200, 200, 100);
  

  pushMatrix();
  translate(200, 200);

  rotate(angle);
  fill(255, 128, 0);
  ellipse(0, -50, 50, 120);
  // ellipse(160,170,50,120);
  popMatrix();
  fill(255, 255, 255);
  stroke(0, 0, 0);
  ellipse(230, 190, 30, 40);
  fill(0,0,0);
  ellipse(235,195,10,10);
  strokeWeight(4);
  line(170,240,140,260);
  ellipse(140,260,20,10);
  line(190,250,160,270);
  ellipse(160,270,20,10);
  
  noStroke();
  fill(255,70,2);
  triangle(250,190,300,190,250,210);
  triangle(250,210,300,210,250,190);
 
  
  //if (frame == 0)
  //{
  //  fill(255,128,0);
  //  ellipse(160,170,50,120);
  //}
  // if(frame ==1)
  //{
  //  pushMatrix();
  //  translate(160 + 50/2,170+ 120/2);

  //  rotate(-PI/16);
  //  fill(255,128,0);
  //   ellipse(0,0,50,120);
  // // ellipse(160,170,50,120);
  //  popMatrix();
  //}
  //if(frame ==2)
  //{
  //  pushMatrix();
  //  translate(160 + 50/2,170+ 120/2);

  //  rotate(-PI/8);
  //  fill(255,128,0);
  //   ellipse(0,0,50,120);
  // // ellipse(160,170,50,120);
  //  popMatrix();
  //}

  //if(frame == 3)
  //{
  //    pushMatrix();
  //  translate(160 + 50/2,170+ 120/2);

  //  rotate(-PI/4);
  //  fill(255,128,0);
  //   ellipse(0,0,50,120);
  //   popMatrix();
  //}
  //  if(frame == 4)
  //{
  //     pushMatrix();
  //  translate(160 + 50/2,170+ 120/2);

  //  rotate(3*-PI/8);
  //  fill(255,128,0);
  //   ellipse(0,0,50,120);
  //   popMatrix();
  //}
  //if(frame < 4)
  //{
  //  frame ++;
  //}
  //else
  //{
  //  frame = 0;
  //}
}
