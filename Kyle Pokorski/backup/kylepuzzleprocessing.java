import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class kylepuzzleprocessing extends PApplet {

ArrayList<Square> squares = new ArrayList<Square>();
int sizex = 4;
int sizey = 4;
float gridwidth = 500;
long time;
long prevtime;
ArrayList<Integer> _values = new ArrayList<Integer>();
boolean trianglemode =true;
public float squarewidth()
{
  if (sizex >= sizey)
  {
    return (float)gridwidth/(float)sizex;
  } else 
  {
    return (float)gridwidth/(float)sizey;
  }
}
public void SwitchValues(int value1, int value2)
{
  int temp1 = value1;
  int temp2 = value2;
  value1 = temp2;
  value2 = temp1;
}
PFont font;
public void setup()
{
  
  font = loadFont("ArialMT-20.vlw");
  if (!trianglemode)
  {
    for (int i = 0; i<sizey; i++)
    {
      for (int j = 0; j<sizex; j++)
      {

        squares.add(new Square(j + i*sizex, new PVector( j* squarewidth(), i* squarewidth()), squarewidth(), 1, sizey*sizex, font, trianglemode,false));
      }
    }
  }
  else
  {
    for(int i = 0; i<sizex; i++)
    {
      
      for(int j = 0; j<2*i + 1; j++)
      {
        
        if(i%2 == 1)
        {
          
            squares.add(new Square(j + i*sizex, new PVector( j* squarewidth()/2, i* squarewidth()), squarewidth(), 1, sizey*sizex, font, trianglemode,true));
        }
        if(i%2 == 0)
        {
            squares.add(new Square(j + i*sizex, new PVector( j* squarewidth()/2, i* squarewidth()), squarewidth(), 1, sizey*sizex, font, trianglemode,false));
        }
        
      }
    }
  }

  _values.clear();
  Scramble(_values);
}
public int switches(ArrayList<Integer> _list)
{
  int temp = 0;
  for (int i = 0; i<sizex*sizey; i++)
  {
    for (int j = i+1; j<sizex*sizey; j++)
    {
      int prev = _list.get(i);
      int next = _list.get(j);
      if (next < prev)
      {
        temp++;
      }
    }
  }
  return temp;
}
public void Scramble(ArrayList<Integer> values)
{

  //ArrayList<Integer> values = new ArrayList<Integer>();
  while (values.size() < sizex*sizey)
  {
    int test = (int)random(0, sizex*sizey);
    if (!values.contains(test))
    {
      values.add(test);
    }
  }
  if (switches(values) %2 == 0)
  {
    for (int i = 0; i<values.size(); i++)
    {

      Square square = squares.get(i);
      int value = values.get(i);
      square.value = value;
      _values = values;
    }
  } else
  {
    Scramble(new ArrayList<Integer>());
  }
}

public void draw()
{
  time = millis() - prevtime;
  background(190, 190, 190);
  fill(0, 0, 0);
  textFont(font);
  text(switches(_values), 0, 520);
  if (keyPressed)
  {
    if (keyCode == DOWN && time > 200)
    {
      for (int i = 0; i<sizex*sizey; i++)
      {
        Square square = squares.get(i);
        if (square.value == (sizex*sizey)-1)
        {
          if (i -sizex >=0)
          {
            Square switchsquare = squares.get(i-sizex);
            int tempvalue1 = square.value;
            int tempvalue2 = switchsquare.value;
            switchsquare.value = tempvalue1;
            square.value = tempvalue2;
          }
        }
      }
      prevtime = millis();
    }
    if (keyCode == UP && time > 200)
    {
      for (int i = 0; i<sizex*sizey; i++)
      {
        Square square = squares.get(i);
        if (square.value == (sizex*sizey)-1)
        {
          if (i +sizex <sizex*sizey)
          {
            Square switchsquare = squares.get(i+sizex);
            int tempvalue1 = square.value;
            int tempvalue2 = switchsquare.value;
            switchsquare.value = tempvalue1;
            square.value = tempvalue2;
          }
        }
      }
      prevtime = millis();
    } 
    if (keyCode == RIGHT && time > 200)
    {
      for (int i = 0; i<sizex*sizey; i++)
      {

        Square square = squares.get(i);
        if (square.value == (sizex*sizey)-1)
        {
          if ((i) %sizex >0)
          {
            Square switchsquare = squares.get(i-1);
            int tempvalue1 = square.value;
            int tempvalue2 = switchsquare.value;
            switchsquare.value = tempvalue1;
            square.value = tempvalue2;
          }
        }
      }
      prevtime = millis();
    }
    if (keyCode == LEFT && time > 200)
    {
      for (int i = 0; i<sizex*sizey; i++)
      {

        Square square = squares.get(i);
        if (square.value == (sizex*sizey)-1)
        {
          if ((i) %sizex < sizex-1)
          {
            Square switchsquare = squares.get(i+1);
            int tempvalue1 = square.value;
            int tempvalue2 = switchsquare.value;
            switchsquare.value = tempvalue1;
            square.value = tempvalue2;
            prevtime = millis();
          }
        }
      }
    }
  }
  for (int i = 0; i<sizex*sizey; i++)
  {
    Square square = squares.get(i);
    square.Draw();
  }
}

class Square
{
  public int value;
  PVector position;
  float Size;
  float strokewidth;
  int numberofsquares;
  PFont font;
  boolean trianglemode = false;
  boolean triangleup = true;
  public Square(int value, PVector position, float Size, float strokewidth, int numberofsquares, PFont font,boolean trianglemode,boolean triangleup)
  {
    this.value = value;
    this.position = position;
    this.strokewidth = strokewidth;
    this.numberofsquares = numberofsquares;
    this.font = font;
    this.Size = Size;
    this.trianglemode = trianglemode;
    this.triangleup = triangleup;
  }
  public void Draw()
  {
    if (value != numberofsquares-1)
    {
      fill(0, 0, (value/(float)numberofsquares)*255);
      strokeWeight(strokewidth);
      stroke(255, 255, 255);
      if(trianglemode)
      {
        if(triangleup)
        {
          
        triangle(position.x,position.y, position.x + Size/2, position.y + Size, position.x +Size, position.y);
        }
        else
        {
          triangle(position.x ,position.y + Size, position.x - Size/2, position.y , position.x +Size, position.y+ Size);
        }
      }
      else
      {
      rect(position.x, position.y, Size, Size);
      }
      strokeWeight(1);
      fill(255, 255, 255);
      textFont(font);

      text(value+1, position.x, position.y + 20);
    }
    else
    {
      noFill();
      stroke(255, 255, 255);
      strokeWeight(strokewidth);
      rect(position.x, position.y, Size, Size);
    }
  }
}

  public void settings() {  size(720, 720); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "kylepuzzleprocessing" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
