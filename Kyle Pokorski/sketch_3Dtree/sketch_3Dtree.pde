float _angle1 = PI/2;
float _angle2 = PI/2;
PVector rotations = new PVector(0, 0, 0);

enum Mode
{
  Line, 
    Box, 
    Sphere
}
Mode mode = Mode.Line;
int branches = 3;
void setup()
{
  size(600, 600, P3D);
}

void draw()
{
  background(190, 190, 190);
  pushMatrix();
  translate(300, 300);
  //directionalLight(255, 30, 255, 1, 0, 0);
  // directionalLight(255, 70, 255, 1, 1, 0);
   //ambientLight(255,255,255);

  if (keyPressed)
  {
    if (key =='z')
    {
      rotations.z-=0.01;
    }
    else if (key =='Z')
    {
      rotations.z+=0.01;
    }
    if (key =='y')
    {
      rotations.y-=0.01;
    }
    else if (key =='Y')
    {
      rotations.y+=0.01;
    }
    if (key =='x')
    {
      rotations.x-=0.01;
    }
    else if (key =='X')
    {
      rotations.x+=0.01;
    }
  }
  _angle1+=0.01f;
  _angle2+=0.01f;
  //rotateX(-mouseY/(float)(height)*TWO_PI);
  //rotateY(-mouseX/(float)(width)*TWO_PI);
  rotateX(rotations.x);
  rotateY(rotations.y);
  rotateZ(rotations.z);
  generate(new PVector(0, 0, 0), 70, 10, 0, _angle1, PI/2, _angle2, PI/2,10);
  popMatrix();
}

void generate(PVector pos, float _length, float strokewidth, int times, float angle1change, float angle1, float angle2change, float angle2,float depth)
{
  //if (times < 5)
  //{

  //}
  if (times <9)
  {

    float newx = pos.x + cos(angle1)*_length;
    float newz = pos.z + sin(angle1)*_length;
    float newy = pos.y + sin(angle2)*_length;
    float Length = sqrt(sq(pos.x-newx) + sq(pos.y-newy) +sq(pos.z-newz));
    if(mode == Mode.Line)
    {
        strokeWeight(strokewidth);
      stroke(0, 0, (255/5)*times);
      
      line(pos.x, pos.y, pos.z, newx , newy , newz);
    }
    pushMatrix();
    translate(pos.x, pos.y, pos.z);
    if (mode == Mode.Sphere)
    {
      sphere(strokewidth);
    } 
    if (mode == Mode.Box)
    {
      //line(0, 0, 0, newx - pos.x, newy - pos.y, newz-pos.z);
      fill(0, 0, (255/5)*times);
      //box( newx - pos.x, newy - pos.y, newz-pos.z);
      rotateX(cos(angle1));
      rotateY(sin(angle2));
     // rotateZ(sin(angle1));
      box(strokewidth,_length,depth);
      
      //box( newx - pos.x, _length, strokewidth);
    }
    //if (mode == Mode.Line)
    //{
    //  strokeWeight(strokewidth);
    //  stroke(0, 0, (255/5)*times);
      
    //  line(0, 0, 0, newx - pos.x, newy - pos.y, newz-pos.z);
    //}
    popMatrix();
    if (branches >= 3)
    {
        generate(new PVector(newx, newy, newz), _length*0.8f, strokewidth*0.8f, times+1, angle1change, angle1+angle1change, angle2change, angle2,depth*0.8f);
      generate(new PVector(newx, newy, newz), _length*0.8f, strokewidth*0.8f, times+1, angle1change, angle1-angle1change, angle2change, angle2,depth*0.8f);
      generate(new PVector(newx, newy, newz), _length*0.8f, strokewidth*0.8f, times+1, angle1change, angle1, angle2change, angle2 -angle2change,depth*0.8f);
      //generate(new PVector(newx, newy, newz), _length*0.8f, strokewidth*0.8f, times+1, angle1change, angle1+angle1change, angle2change, angle2,depth*0.8f);
      //generate(new PVector(newx, newy, newz), _length*0.8f, strokewidth*0.8f, times+1, angle1change, angle1-angle1change, angle2change, angle2,depth*0.8f);
      //generate(new PVector(newx, newy, newz), _length*0.8f, strokewidth*0.8f, times+1, angle1change, angle1, angle2change, angle2 -angle2change,depth*0.8f);
    }
    if (branches >= 4)
    {
      // generate(new PVector(newx, newy, newz), _length*0.7f, strokewidth*0.7f, times+1, angle1change, angle1, angle2change, angle2 +angle2change);
      generate(new PVector(newx, newy, newz), _length*0.8f, strokewidth*0.7f, times+1, angle1change, angle1-angle1change, angle2change, angle2 -angle2change,depth*0.8f);
    }
    if (branches >= 5)
    {
      generate(new PVector(newx, newy, newz), _length*0.8f, strokewidth*0.7f, times+1, angle1change, angle1+angle1change, angle2change, angle2 -angle2change,depth*0.8f);
    }
  }
}
