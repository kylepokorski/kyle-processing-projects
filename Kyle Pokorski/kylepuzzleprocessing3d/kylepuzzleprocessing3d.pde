ArrayList<Square> squares = new ArrayList<Square>(); //<>// //<>// //<>// //<>//
int sizex = 2;
int sizey = 2;
int sizez = 2;
//float gridwidth = 720;

float gridwidth = 400;
int time;
int prevtime;
boolean dowalls = false;
int walls = 1;
boolean IsSolved = false;
boolean trianglemode = false;
int moves = 0;
float offset = 0;
boolean enable3d = true;
float rotationsx = PI/2;
float rotationsy =0;


void CheckSolved()
{
  int inarow = 0;
  for (int i = 0; i<squares.size(); i++)
  {
    Square square = squares.get(i);
    if (square.value == i)
    {
      inarow++;
    } else
    {
      break;
    }
  }
  if (inarow == squares.size())
  {
    IsSolved = true;
  }
}
public float squarewidth()
{
  if (sizex >= sizey)
  {
    return (float)gridwidth/(float)sizex;
  } else 
  {
    return (float)gridwidth/(float)sizey;
  }
}
public void SwitchValues(int value1, int value2)
{
  int temp1 = value1;
  int temp2 = value2;
  value1 = temp2;
  value2 = temp1;
}
PFont font;
public void setup()
{

  size(760, 760, P3D);
  //size(760, 760);

  if (enable3d)
  {
  } else
  {
  }
  font = loadFont("ArialMT-20.vlw");
  if (!trianglemode)
  {
    if (!enable3d)
    {
      for (int i = 0; i<sizey; i++)
      {
        for (int j = 0; j<sizex; j++)
        {

          squares.add(new Square(j + i*sizex, new PVector( j* squarewidth(), i* squarewidth()), squarewidth(), 1, sizey*sizex, font, false, false, 1, false));
        }
      }
    } else
    {
      for (int h = 0; h <sizez; h++)
      {
        for (int i = 0; i<sizey; i++)
        {
          for (int j = 0; j<sizex; j++)
          {

            squares.add(new Square(j + i*sizex + h*sizex*sizey, new PVector( j* squarewidth(), i* squarewidth(), h*squarewidth()), squarewidth(), 1, sizey*sizex*sizez, font, false, false, 1, true));
          }
        }
      }
    }
  }
  if (trianglemode)
  {
    //for (int i = 0; i<sizey; i++)
    //{
    //  for (int j = 0; j<sizex; j++)
    //  {

    //    squares.add(new Square(j + i*sizex , new PVector( j* squarewidth(), i* squarewidth()  +squarewidth()), squarewidth(), 1, sizey*sizex, font, true, true));
    //  }
    //}
    sizey = sizex;
    offset = -squarewidth()/2;

    for (int i = 0; i<sizex; i++)
    {
      for (int j = 0; j<2*i + 1; j++)
      {
        if (j%2 == 0)
        {
          squares.add(new Square(squares.size(), new PVector(gridwidth/2+ j* squarewidth()/2 + offset, i* squarewidth()  +squarewidth()), squarewidth(), 1, sizey*sizex, font, true, true, i, false));
        }
        if (j%2 == 1)
        {
          squares.add(new Square(squares.size(), new PVector(gridwidth/2+ j* squarewidth()/2 + offset, i* squarewidth()  +squarewidth()), squarewidth(), 1, sizey*sizex, font, true, false, i, false));
        }
      }
      offset-=squarewidth()/2;
    }
  }
  Scramble(0);
}
int switches(ArrayList<Integer> _values)
{
  int temp = 0;
  for (int i = 0; i<_values.size(); i++)
  {
    for (int j = i+1; j<_values.size(); j++)
    {
      int valueati = _values.get(i);
      int valueatj = _values.get(j);
      if (valueati > valueatj)
      {
        temp++;
      }
    }
  }
  return temp;
}
void Scramble(int times)
{
  IsSolved = false;
  for (int i = 0; i<squares.size(); i++)
  {
    Square square = squares.get(i);
    square.value = i;
    square.showtile = false;
  }
  if (times < 5)
  {

    ArrayList<Integer> values = new ArrayList<Integer>();
    if (dowalls)
    {
      ArrayList<Integer> _walls = new ArrayList<Integer>();

      while (_walls.size() < walls)
      {
        int temp = (int)random(0, squares.size());
        if (!_walls.contains(temp))
        {
          _walls.add(temp);
          Square square = squares.get(temp);
          square.iswall = true;
        }
      }
      while (values.size() < squares.size() -walls)
      {
        int temp = (int)random(0, squares.size() );
        if (!values.contains(temp)  )
        {
          Square square = squares.get(temp);
          if (!square.iswall)
          {
            values.add(temp);
          } else
          {
            if (!values.contains(square.value))
            {
              //values.add(square.value,square.value);
            }
            //values.add(temp);
            //if(!values.contains(
          }
        }
      }

      for (int i = 0; i<squares.size(); i++)
      {

        // int temp = _walls.get(i);
        Square square = squares.get(i);
        //square.iswall = true;
        if (square.iswall)
        {

          values.add(i, i);
        }
      }
    } else
    {
      if (trianglemode)
      {
        while (values.size() < squares.size()-3)
        {
          int temp = (int)random(0, squares.size() );
          if (!values.contains(temp) && temp != 0 && temp != sq(sizex-1) && temp!= sq(sizex)-1)
          {
            values.add(temp);
          }
        }
        for (int i = 0; i<squares.size(); i++)
        {
          if (!values.contains(i))
          {
            values.add(i, i);
          }
        }
      } else
      {

        while (values.size() < squares.size())
        {
          int temp = (int)random(0, squares.size() );
          if (!values.contains(temp))
          {
            values.add(temp);
          }
        }
      }
    }

    if (switches(values)%2 ==0)
    {
      for (int i = 0; i<squares.size(); i++)
      {
        int value =values.get(i);
        Square square = squares.get(i);
        square.value = value;
      }
    } else
    {

      Scramble(times+1);
    }
  } else
  {
    for (int i = 0; i<squares.size(); i++)
    {
      Square square = squares.get(i);
      square.value = i;
    }
  }
}

public void draw()
{
  time = millis() - prevtime;
  background(190, 190, 190);
  CheckSolved();
  fill(0, 0, 0);
  textFont(font);
  text(moves, 50, gridwidth +20);
  if (keyPressed)
  {
    if (key == 'a')
    {
      rotationsx -= 0.01f;
    }
    if (key == 'w')
    {
      rotationsy -= 0.01f;
    }
  }
  if (!IsSolved)
  {
    if (keyPressed)
    {
      if (!trianglemode)
      {
        if (keyCode == DOWN && time > 200)
        {
          for (int i = 0; i<squares.size(); i++)
          {
            Square square = squares.get(i);
            if (square.value == (squares.size())-1)
            {
              if (i >=sizex)
              {

                Square switchsquare = squares.get(i-sizex);
                if (!square.iswall && !switchsquare.iswall)
                {
                  moves++;
                  int tempvalue1 = square.value;
                  int tempvalue2 = switchsquare.value;

                  switchsquare.value = tempvalue1;
                  square.value = tempvalue2;
                }
              }
            }
          }
          prevtime = millis();
        } else if (keyCode == UP && time > 200)
        {
          for (int i = 0; i<squares.size(); i++)
          {
            Square square = squares.get(i);
            if (square.value == squares.size()-1)
            {
              if (i +sizex <=squares.size() -1 )
              {
                if (i % (sizex*sizey) < sizex*sizey   && enable3d || !enable3d)
                {
                  Square switchsquare = squares.get(i+sizex);
                  if (!switchsquare.iswall && !square.iswall)
                  {
                    moves++;
                    int tempvalue1 = square.value;
                    int tempvalue2 = switchsquare.value;
                    switchsquare.value = tempvalue1;
                    square.value = tempvalue2;
                    break;
                  }
                }
              }
            }
          }
          prevtime = millis();
        } else if (keyCode == RIGHT && time > 200)
        {
          for (int i = 0; i<squares.size(); i++)
          {

            Square square = squares.get(i);
            if (square.value == squares.size()-1)
            {
              if ((i) %sizex >0)
              {
                Square switchsquare = squares.get(i-1);
                if (!square.iswall && !switchsquare.iswall)
                {
                  moves++;
                  int tempvalue1 = square.value;
                  int tempvalue2 = switchsquare.value;
                  switchsquare.value = tempvalue1;
                  square.value = tempvalue2;
                  break;
                }
              }
            }
          }
          prevtime = millis();
        } else if (keyCode == LEFT && time > 200)
        {
          for (int i = 0; i<squares.size(); i++)
          {

            Square square = squares.get(i);
            if (square.value == squares.size()-1)
            {

              if ((i) %sizex <sizex-1)
              {

                Square switchsquare = squares.get(i+1);
                if (!square.iswall && !switchsquare.iswall)
                {
                  moves++;
                  int tempvalue1 = square.value;
                  int tempvalue2 = switchsquare.value;
                  switchsquare.value = tempvalue1;
                  square.value = tempvalue2;
                  break;
                }
              }
            }
          }
          prevtime = millis();
        } else if (key == 'k' && time > 200 && enable3d)
        {
          for (int i = 0; i<squares.size(); i++)
          {

            Square square = squares.get(i);
            if (square.value == squares.size()-1)
            {

              if ((i + sizex*sizey)  <squares.size()-1)
              {

                Square switchsquare = squares.get(i+sizex*sizey);
                if (!square.iswall && !switchsquare.iswall)
                {
                  moves++;
                  int tempvalue1 = square.value;
                  int tempvalue2 = switchsquare.value;
                  switchsquare.value = tempvalue1;
                  square.value = tempvalue2;
                  break;
                }
              }
            }
          }
          prevtime = millis();
        }
        else if (key == 'l' && time > 200 && enable3d)
        {
          for (int i = 0; i<squares.size(); i++)
          {

            Square square = squares.get(i);
            if (square.value == squares.size()-1)
            {

              if ((i - sizex*sizey)  >=0)
              {

                Square switchsquare = squares.get(i-sizex*sizey);
                if (!square.iswall && !switchsquare.iswall)
                {
                  moves++;
                  int tempvalue1 = square.value;
                  int tempvalue2 = switchsquare.value;
                  switchsquare.value = tempvalue1;
                  square.value = tempvalue2;
                  break;
                }
              }
            }
          }
          prevtime = millis();
        }
      } else
      {
        if (keyCode == LEFT && time > 200)
        {
          for (int i = 0; i<squares.size(); i++)
          {

            Square square = squares.get(i);
            if (square.value == squares.size()-1)
            {

              if (i+1 < squares.size())
              {

                Square switchsquare = squares.get(i+1);
                if (!square.iswall && !switchsquare.iswall && square.row == switchsquare.row)
                {
                  moves++;
                  int tempvalue1 = square.value;
                  int tempvalue2 = switchsquare.value;
                  switchsquare.value = tempvalue1;
                  square.value = tempvalue2;
                  break;
                }
              }
            }
          }
          prevtime = millis();
        } else if (keyCode == RIGHT && time > 200)
        {
          for (int i = 0; i<sizex*sizey; i++)
          {

            Square square = squares.get(i);
            if (square.value == (sizex*sizey)-1)
            {
              if (i-1 > 0)
              {
                Square switchsquare = squares.get(i-1);
                if (!square.iswall && !switchsquare.iswall && square.row == switchsquare.row)
                {
                  moves++;
                  int tempvalue1 = square.value;
                  int tempvalue2 = switchsquare.value;
                  switchsquare.value = tempvalue1;
                  square.value = tempvalue2;
                  break;
                }
              }
            }
          }
          prevtime = millis();
        } else if (keyCode == UP && time > 200)
        {
          for (int i = 0; i<squares.size(); i++)
          {
            Square square = squares.get(i);
            if (square.value == squares.size() -1)
            {
              int row = square.row;

              if ( square.facingup && i + (row+1)*2 < squares.size())
              {
                Square switchsquare = squares.get(i+(row+1)*2);
                if (!square.iswall && !switchsquare.iswall)
                {
                  moves++;
                  int tempvalue1 = square.value;
                  int tempvalue2 = switchsquare.value;
                  switchsquare.value = tempvalue1;
                  square.value = tempvalue2;
                  break;
                }
              }
            }
          }
          prevtime = millis();
        } else if (keyCode == DOWN && time > 200)
        {
          for (int i = 0; i<squares.size(); i++)
          {
            Square square = squares.get(i);
            if (square.value == squares.size() -1)
            {
              int row = square.row;

              if ( !square.facingup && i - (row)*2 >= 0)
              {
                Square switchsquare = squares.get(i-(row)*2);
                if (!square.iswall && !switchsquare.iswall)
                {
                  moves++;
                  int tempvalue1 = square.value;
                  int tempvalue2 = switchsquare.value;
                  switchsquare.value = tempvalue1;
                  square.value = tempvalue2;
                  break;
                }
              }
            }
          }
          prevtime = millis();
        }
      }
    }
  }
  if (IsSolved)
  {
    textFont(font);
    fill(0, 0, 0);
    text("You solved the puzzle!", 100, gridwidth + 20);
    Square lastsquare = squares.get(squares.size()-1);
    lastsquare.showtile = true;
    //circle(
  }
  if (!enable3d)
  {
    for (int i = 0; i<sizex*sizey; i++)
    {

      Square square = squares.get(i);
      square.Draw();
    }
  } else
  {
    pushMatrix();
    translate(width/2, height/2,-width/2 );
    //ambientLight(255, 0, 0);
    rotateX(rotationsx);
    rotateY(rotationsy);

    pushMatrix();

    fill(0, 200, 0);
    noStroke();
    translate(-45, -45, -45);

    sphere(15);

    popMatrix();

    for (int i = 0; i<squares.size(); i++)
    {
      pushMatrix();
      
      Square square = squares.get(i);

      square.Draw();
      popMatrix();
    }
     popMatrix();
    
  }
  
}
