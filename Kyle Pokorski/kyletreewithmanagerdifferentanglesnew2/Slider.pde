public static ArrayList<Slider> allsliders = new ArrayList<Slider>();
public class Slider
{
  PVector position;
  float _slidermin;
  float _slidermax;
  float Size;
  float Width;
  boolean IsVertical;
  float sliderposition;
  String screen;
  boolean IsVisible; 
  public boolean IsLocked = false;

  public float slidedistancepercentage()
  {
    return (sliderposition - _slidermin)/(_slidermax -_slidermin);
  }
  public void MoveSlider(float percentage)
  {
    sliderposition = percentage*(_slidermax-_slidermin)  +_slidermin ;
  }
  public Slider (PVector pos, float slidermin, float slidermax, float sliderposition, float size, float _Width, boolean IsVertical, String screen)
  {
    position = pos;
    _slidermin = slidermin;
    _slidermax = slidermax;
    Size = size;
    Width = _Width;
    this.screen = screen;
    this.sliderposition =sliderposition;
    allsliders.add(this);
    this.IsVertical = IsVertical;
  }
  public void Draw()
  {
    if (IsVertical)
    {
      stroke(255, 255, 255);
      strokeWeight(Width);

      line(position.x, _slidermin, position.x, _slidermax);

      strokeWeight(1);
      fill(128, 128, 128);
      ellipse(position.x, sliderposition, Size, Size);
    } else
    {
      stroke(255, 255, 255);
      strokeWeight(Width);

      line(_slidermin, position.y, _slidermax, position.y);
      strokeWeight(1);
      fill(128, 128, 128);
      ellipse(sliderposition, position.y, Size, Size);
    }
  }
  public void Update()
  {



    if (InBounds() && Global.manager.screentype == screen && slidedistancepercentage() >= 0 && slidedistancepercentage() <= 1 && !IsLocked )
    {
      if (!IsVertical)
      {
        sliderposition = mouseX;
      } else
      {
        sliderposition = mouseY;
      }
    }
    if (slidedistancepercentage() >1)
    {
      MoveSlider(1);
    }
    if (slidedistancepercentage() < 0)
    {
      MoveSlider(0);
    }
  }

  public boolean InBounds()
  {
    if (!IsVertical)
    {
      if (sqrt(sq(mouseX -sliderposition) + sq(mouseY - position.y)) <= Size)
      {
        return true;
      } else
      {
        return false;
      }
    } else
    {
      if (sqrt(sq(mouseX -position.x) + sq(mouseY - sliderposition)) <= Size)
      {
        return true;
      } else
      {
        return false;
      }
    }
  }
}

public static void GlobalUpdate()
{
  for (Slider s : allsliders)
  {
    if (s.screen == Global.manager.screentype)
    {
      s.Update();
    }
  }
}
