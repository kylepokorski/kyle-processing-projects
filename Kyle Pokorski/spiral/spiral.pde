void setup()
{
  size(500, 500);
}
void draw()
{
  generate(0, 10, new PVector(300, 300), PI/2);
}
void generate(int times, float size, PVector pos, float angle)
{
  if (times < 30)
  {

    float newx = pos.x+cos(angle)*size;
    float newy = pos.y+ sin(angle)*size;

    rect(pos.x, pos.y, size, size);
    float movex;
    if(angle%(PI*2) <PI)
    {
      movex= size;
    }
    else
    {
      movex=-size;
    }
    if (times ==0)
    {
      generate(times +1, size, new PVector(newx, newy), angle + PI/2);
    } 
    else
    {
      if (times %2 == 0)
      {
        //newy = newy + size;
        generate(times +1, floor(size), new PVector(newx+movex, newy), angle + PI/2);
      }
      if (times%2 == 1)
      {
        //newx = newx+size;
        
        generate(times +1, ceil(size), new PVector(newx+movex, newy), angle + PI/2);
      }
    }
  }
}
