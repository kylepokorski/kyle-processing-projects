//public static ScreenManager manager;
MainScreen main;
SettingsScreen settings;
void setup()
{
  size(600, 600);
  Global.manager = new ScreenManager();
  main = new MainScreen(color(0, 255, 255), "Main");
  settings = new SettingsScreen(main);
  Global.manager.Add(main);
  Global.manager.Add(settings);
  Global.manager.screentype = "Main";
}
void draw()
{
  Global.manager.Draw();
}
void mouseClicked()
{

  if (mouseX >= 375 && mouseX <= 425 && mouseY >= 0 && mouseY <= 50 && Global.manager.screentype == "Main")
  {
    Global.manager.screentype = "Settings";
  }
   if ( mouseY >= 300&& Global.manager.screentype == "Settings")
  {
    Global.manager.screentype = "Main";
  }
}
