public class MainScreen extends Screen
{
  public float _lengthdecrease = 0.8f;
  float angle = 0;
  public MainScreen(color backgroundcolor, String screenName)

  {
    super(backgroundcolor, screenName);
  }
 
  public void Draw()
  {

    super.Draw();
    if (angle < PI)
    {
    }
    angle+=0.01f;


    
    stroke(0, 0, 0);
    fill(255, 255, 255);
    ellipse(400, 25, 50, 50);

    fill(0, 0, 0);
    ellipse(400, 25, 25, 25);
    strokeWeight(5);
    line(400, 7, 400, 43);
    







    generate(new PVector(400, 400), 50, PI/2, angle, 0, 10, _lengthdecrease);
  }
  void generate(PVector point, float _length, float angle, float anglechange, int times, int strokewidth, float lengthdecrease)
  {
    if (times <7)
    {

      strokeWeight(strokewidth);
      stroke(0, 0, times *(255/6));
      float newx = point.x + cos(angle)*_length ;
      float newy = point.y - _length *sin(angle);
      line(point.x, point.y, newx, newy);
      generate(new PVector(newx, newy), _length*lengthdecrease, angle - anglechange, anglechange, times+1, (int)(strokewidth*lengthdecrease), lengthdecrease);
      generate(new PVector(newx, newy), _length*lengthdecrease, angle + anglechange, anglechange, times+1, (int)(strokewidth*lengthdecrease), lengthdecrease);
    }
  }
}
