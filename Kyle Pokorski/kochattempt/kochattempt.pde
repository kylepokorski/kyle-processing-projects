void setup()
{
  size(500, 500);
}
void draw()
{
  generate(new PVector(50, 100), 0, 200, 0);
}
void generate(PVector pos, float angle, float _length, int times)
{
  if (times < 2)
  {
    float newx = pos.x+ cos(angle)*_length;
    float newy =pos.y+ sin(angle)*_length;
    line(pos.x, pos.y, newx, newy);
    //background(255, 255, 255);
    generate(new PVector(pos.x + _length/3,pos.y),angle - sqrt(3)/2,_length/3,times+1);
    generate(new PVector(pos.x+2*_length/3 ,pos.y - _length/3 +5),  sqrt(3)/2,_length/3,times+1);
  }
}
