PVector [] rotations = new PVector[9];
void setup()
{
  size(600, 600, P3D);
}
void DrawRotatingBox(PVector pos, int i)
{
  pushMatrix();
  translate(pos.x, pos.y);
  float distance = sqrt(sq(mouseY - pos.x) + sq(mouseY - pos.y));
  if (mouseX - pos.x <= 80 && mouseX - pos.x >= 0 && mouseY - pos.y <= 80 && mouseX -pos.x >= 0)
  {
  }
  
  rotations[i] = new PVector((-mouseY+pos.y)/(float)(width)*2*PI, (-mouseX+pos.x)/(float)(height)*2*PI);
    rotateX(rotations[i].x);
    rotateY(rotations[i].y);
  //if (distance <= 100)
  //{
    
  //} else
  //{
  //  if (rotations[i] != null)
  //  {
  //    rotateX(rotations[i].x);
  //    rotateY(rotations[i].y);
  //  }
  //}
  // fill(0, 0, 255);

  box(40);
  popMatrix();
}
void draw()
{
  background(255, 255, 255);
  ambientLight(100, 255, 255);
  directionalLight(130, 130, 140, 1, 0, 0);
  for (int i = 0; i<3; i++)
  {
    for (int j = 0; j<3; j++)
    {
      DrawRotatingBox(new PVector(j*80 + 50, i*80+50), i + j*3);
    }
  }

  //pushMatrix();
  //translate(350,250);


  // rotateX(-mouseY/(float)(200)*2*PI);
  //rotateY(-mouseX/(float)(200)*2*PI);


  //fill(0,0,255);
  //box(30);
  //popMatrix();
}
