ArrayList<Circle> circles = new ArrayList<Circle>();
boolean gameRun = true;
float sum;
PFont font;
int lastKeyCode;
void Reset()
{
  //int seed = round(random(0,round(pow(2,32))));
  // randomSeed(seed);
  gameRun = true;
  circles.clear();
  //circles.add(new Circle(new PVector(150, 150), new PVector(1.5f, -1.8f), 70, color(100, 100, 100)));

  //circles.add(new Circle(new PVector(100, 100), new PVector(-1.6f, -1.5f), 70, color(50, 50, 50)));
  for (Integer i = 0; i< 92; i++)
  {

    float channel = random(0, 195);
    PVector pos = new PVector(random(70/2, 960 - 70/2), random(70/2, 720-70/2));
    PVector speed = new PVector(random(-1, 1)*0.4f, random(-1, 1)*0.4f);

    for (int j  = 0; j<circles.size(); j++)
    {
      Circle circle = circles.get(j);

      if (sqrt(pow(pos.x -circle.pos.x, 2) + pow(pos.y-circle.pos.y, 2)) < circle.size/2+ 70/2)
      {
        pos = new PVector(random(70/2, 960 - 70/2), random(70/2, 720-70/2));
        j = -1;
      }
    }
    //while (-0.2<speed.x   && speed.x<0.2)
    //{
    //  speed.x = random(random(-1, 1)*2.9f);
    //}
    //while (-0.2<speed.y   && speed.y<0.2)
    //{
    //  speed.y = random(random(-1, 1)*2.9f);
    //}
    circles.add(new Circle(pos, speed, 70, color(channel, channel, channel)));
    Circle lastcircle = circles.get(i);
    lastcircle.posinlist = i;
  }
}
void setup()
{
  pixelDensity(2);
  size(960, 720);
  Reset();

  font = loadFont("Arial-BoldMT-32.vlw");
}
void draw()
{

  background(235, 235, 235);
  fill(0, 0, 0);
  noStroke();
  textFont(font);
  text(round(sum), 0, 32);
  sum = 0;
 
  for (Circle circle : circles)
  {


   sum += circle.number;
    circle.Draw();
    if (gameRun)
    {

      circle.Update();
      if (mousePressed)
      {
        if (sqrt(pow(mouseX - circle.pos.x, 2)+pow((mouseY -circle.pos.y), 2)) < circle.size/2)
        {

          if (circle.number < 100)
          {

            float newsize = circle.size+3;
            if (circle.pos.x - (newsize/2) > 0 && circle.pos.y - (newsize/2) > 0 && circle.pos.x + (newsize/2) <width && circle.pos.y + (newsize/2) < height)
            {
              circle._color = color(230, 0, 0);
              circle.size+=3;
            }
            if (circle.pos.x - (newsize/2) < 0)
            {
              circle.speed.x +=0.01f;
            }
            if (circle.pos.y - (newsize/2) < 0)
            {
              circle.speed.y +=0.01f;
            }
            if (circle.pos.x + (newsize/2) > 600)
            {
              circle.speed.x -=0.01f;
            }
            if (circle.pos.y + (newsize/2) > 600)
            {
              circle.speed.y -=0.01f;
            }
          }
        } else
        {
          circle._color = circle._originalcolor;
        }
      } else
      {
        circle._color = circle._originalcolor;
      }


      for (Circle othercircle : circles)
      {
        if (othercircle != circle)
        {
          if (sqrt(pow(circle.pos.x- othercircle.pos.x, 2) + pow(circle.pos.y - othercircle.pos.y, 2)) <= circle.size/2+othercircle.size/2)
          {
            if (circle._color == color(230, 0, 0))
            {

              gameRun = false;
            } else
            {
              PVector oldspeed1 = circle.speed;
              PVector oldspeed2 = othercircle.speed;

//swap speeds method
              circle.speed = oldspeed2;
              othercircle.speed = oldspeed1;
              

              circle.pos.x += circle.speed.x;
              circle.pos.y += circle.speed.y;
             othercircle.pos.x +=othercircle.speed.x;
              othercircle.pos.y += othercircle.speed.y;
              //if(circle.pos.x - circle.size/2 < othercircle.pos.x)
              //{
              //  circle.speed.x = -abs(circle.speed.x);
              //  //othercircle.speed.x = abs(othercircle.speed.x);
              //}
              //if(circle.pos.x + circle.size/2 > othercircle.pos.x)
              //{
              //  circle.speed.x = abs(circle.speed.x);
              //}
              //if(circle.pos.y - circle.size/2 < othercircle.pos.y)
              //{
              //  circle.speed.y = -abs(circle.speed.y);
              //}
              //if(circle.pos.y + circle.size/2 > othercircle.pos.y)
              //{
              //  circle.speed.y = abs(circle.speed.y);
              //}
            }
          }
        }
      }
    }
  }
  if (!gameRun)
  {

    if (keyCode == ' ' && lastKeyCode != ' ')
    {
      Reset();
    }
  }
  lastKeyCode = keyCode;
}
//void mouseDragged()
//{
//  for(Circle circle: circles)
//  {
//    if(sqrt(pow(mouseX - circle.pos.x,2)+pow((mouseY -circle.pos.x),2)) < circle.size)
//    {

//      if(circle.number < 100)
//      {

//        if(circle.pos.x - ((circle.size+5)/2) > 0)
//        {
//          circle._color = color(255,0,0);
//      circle.size+=5;
//        }
//      }
//    }

//  }
//}
