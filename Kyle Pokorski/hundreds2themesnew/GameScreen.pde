public class GameScreen extends Screen
{
  ArrayList<Circle> circles = new ArrayList<Circle>();
  boolean gameRun = true;
  float sum;
  PFont font;
  int lastKeyCode;
  RectangleObstacle rect;
  Circle collidedcircle1;
  Circle collidedcircle2;
  float alpha = 0;
  boolean holdtogrow = true;
  int theme = 0;
 // color backgroundColor = color(235, 235, 235);
  color textcolor = color(0);
  color mincirclecolor = color(0, 0, 0);
  color maxcirclecolor = color(195, 195, 195);
  // public color selectedcolor = color(230,0,0);
  //0 - classic
  //1
  //2 - 
  void Reset()
  {
    setTheme();
    alpha = 0;
    //int seed = round(random(0,round(pow(2,32))));
    // randomSeed(seed);
    gameRun = true;
    circles.clear();
    //circles.add(new Circle(new PVector(150, 150), new PVector(1.5f, -1.8f), 70, color(100, 100, 100)));

    //circles.add(new Circle(new PVector(100, 100), new PVector(0,0), 70, color(50, 50, 50)));
    //Circle circlesta = circles.get(0);
    //circlesta.isstatic = true;

    // circles.add(new Circle(new PVector(130, 200), new PVector(-2.5f, -1.8f), 70, color(100, 100, 100)));

    //circles.add(new Circle(new PVector(190, 400), new PVector(1.5f, 1f), 70, color(100, 100, 100), circles.get(0))); 

    //circles.add(new Circle(new PVector(300, 300), new PVector(0, 0), 300, color(0, 0, 0)));
    //circles.get(0).isstatic = true;
    for (Integer i = 0; i<20; i++)
    {


      float interpolate = random(0, 1);
      PVector pos = new PVector(random(72/2, 960 - 72/2), random(72/2, 720-72/2));
      PVector speed = new PVector(random(-1, 1)*3.9f, random(-1, 1)*3.9f);
     int shrink = (int)random(0,2);

      for (int j  = 0; j<circles.size(); j++)
      {
        Circle circle = circles.get(j);

        if (sqrt(pow(pos.x -circle.pos.x, 2) + pow(pos.y-circle.pos.y, 2)) < circle.size/2+ 72/2)
        {
          pos = new PVector(random(72/2, 960 - 72/2), random(72/2, 720-72/2));
          j = -1;
        }
      }
      while (-0.2<speed.x   && speed.x<0.2)
      {
        speed.x = random(random(-1, 1)*3.9f);
      }
      while (-0.2<speed.y   && speed.y<0.2)
      {
        speed.y = random(random(-1, 1)*1.9f);
      }
      circles.add(new Circle(pos, speed, 72, lerpColor(mincirclecolor, maxcirclecolor, interpolate)));
      Circle lastcircle = circles.get(i);
      //if(shrink == 0)
      //{
      //  lastcircle.shrink = false;
      //}
      //else
      //{
      //  lastcircle.shrink = true;
      //}
      lastcircle.posinlist = i;
    }
    // rect = new RectangleObstacle(new PVector(100, 100), 50, 150, circles);
  }
  public GameScreen()
  {
    super(color(235, 235, 235), "Game");

    Reset();
    font = loadFont("Arial-BoldMT-32.vlw");
  }
  void setTheme()
  {
    if (theme == 4)
    {
      Global.gamebackgroundcolor = color(20);
      mincirclecolor = color(255);
      maxcirclecolor = color(60);
      textcolor = color(255);
      Global.selectedcolor = color(230, 0, 0);
    }
    if (theme == 3)
    {
      Global.gamebackgroundcolor = color(0, 191, 255);
      mincirclecolor = color(100, 64, 0);
      maxcirclecolor = color(0, 128, 0);
      Global.selectedcolor = color(255, 0, 255);
      textcolor = color(0);
    }
    if (theme == 2)
    {
      Global.gamebackgroundcolor = color(190, 190, 190);
      textcolor = color(0, 0, 0);
      mincirclecolor = color(0, 0, 0);
      maxcirclecolor = color(0, 0, 255);
      Global.selectedcolor = color(255, 255, 0);
    }
    if (theme == 1)
    {
      Global.gamebackgroundcolor = color(0, 0, 0);
      textcolor = color(0, 255, 0);
      mincirclecolor = color(0, 50, 0);
      maxcirclecolor = color(0, 230, 0);
      Global.selectedcolor = color(250, 200, 0);
    }
    if (theme == 0)
    {
      Global.gamebackgroundcolor = color(235);
      textcolor = color(0);
      Global.selectedcolor = color(230, 0, 0);
      mincirclecolor = color(0);
      maxcirclecolor = color(195);
    }
    backgroundcolor = Global.gamebackgroundcolor;
  }
  public void Draw()
  {

    super.Draw();
    fill(textcolor);
    noStroke();
    textFont(font);
    text(round(sum), 0, 32);
    sum = 0;
    // rect.Update();
    //rect.Draw();


    for (Circle circle : circles)
    {


      sum += circle.number;
      circle.Draw();
      if (gameRun)
      {

        circle.Update();

        if (mousePressed && !circle.isstatic && holdtogrow || !holdtogrow)
        {
          if (sqrt(pow(mouseX - circle.pos.x, 2)+pow((mouseY -circle.pos.y), 2)) < circle.size/2)
          {

            if (round(circle.number) < 100)
            {

              float newsize = circle.size+6.4;
              if (circle.pos.x - (newsize/2) > 0 && circle.pos.y - (newsize/2) > 0 && circle.pos.x + (newsize/2) <width && circle.pos.y + (newsize/2) < height)
              {
                circle._color = Global.selectedcolor;
                circle.size+=6.4;
                if (circle.linkedcircle != circle)
                {
                  circle.linkedcircle._color = Global.selectedcolor;
                  circle.linkedcircle.size+=6.4;
                }
              }
              if (circle.pos.x - (newsize/2) < 0 )
              {
                circle.speed.x +=0.01f;
                // circle.speed.x = abs(circle.speed.x);
              }
              if (circle.pos.y - (newsize/2) < 0 )
              {
                circle.speed.y +=0.01f;
                //circle.speed.y = abs(circle.speed.y);
              }
              if (circle.pos.x + (newsize/2) > width  )
              {
                circle.speed.x -=0.01f;
                // circle.speed.x = -abs(circle.speed.x);
              }
              if (circle.pos.y + (newsize/2) > height )
              {
                circle.speed.y -=0.01f;
                // circle.speed.y = -abs(circle.speed.y);
              }
            }
          } else
          {
            circle._color = circle._originalcolor;
          }
        } else
        {
          if (circle.linkedcircle != circle && circle.linkedcircle._color == Global.selectedcolor)
          {
          } else
          {
            circle._color = circle._originalcolor;
          }
        }

        if (mousePressed && !circle.linkedcircle.isstatic && circle != circle.linkedcircle && holdtogrow||!holdtogrow &&  !circle.linkedcircle.isstatic && circle != circle.linkedcircle)
        {
          if (sqrt(pow(mouseX - circle.linkedcircle.pos.x, 2)+pow((mouseY -circle.linkedcircle.pos.y), 2)) < circle.linkedcircle.size/2)
          {

            if (circle.linkedcircle.number < 100)
            {

              float newsize = circle.linkedcircle.size+6.4;
              if (circle.linkedcircle.pos.x - (newsize/2) > 0 && circle.linkedcircle.pos.y - (newsize/2) > 0 && circle.linkedcircle.pos.x + (newsize/2) <width && circle.linkedcircle.pos.y + (newsize/2) < height)
              {
                circle.linkedcircle._color = Global.selectedcolor;
                //circle.linkedcircle.size+=3;
                if (circle.linkedcircle != circle)
                {
                  circle._color = Global.selectedcolor;
                  circle.size+=6.4;
                }
              }
              if (circle.linkedcircle.pos.x - (newsize/2) < 0)
              {
                circle.linkedcircle.speed.x +=0.01f;
              }
              if (circle.linkedcircle.pos.y - (newsize/2) < 0)
              {
                circle.linkedcircle.speed.y +=0.01f;
              }
              if (circle.linkedcircle.pos.x + (newsize/2) > 600)
              {
                circle.linkedcircle.speed.x -=0.01f;
              }
              if (circle.linkedcircle.pos.y + (newsize/2) > 600)
              {
                circle.linkedcircle.speed.y -=0.01f;
              }
            }
          } else
          {
            //circle.linkedcircle._color = circle.linkedcircle._originalcolor;
          }
        } else
        {
          //if(circle != circle.linkedcircle)
          //{
          //circle.linkedcircle._color = circle.linkedcircle._originalcolor;
          //}
        }


        for (Circle othercircle : circles)
        {
          if (othercircle != circle)
          {

            PVector newpos1 = new PVector(circle.pos.x+circle.speed.x, circle.pos.y+circle.speed.y);
            PVector newpos2 = new PVector(othercircle.pos.x+othercircle.speed.x, othercircle.pos.y+othercircle.speed.y);

            if (sqrt(pow(newpos1.x - newpos2.x, 2) + pow(newpos1.y-newpos2.y, 2)) <= circle.size/2 + othercircle.size/2)
            {


              PVector oldspeed1 = circle.speed;
              PVector oldspeed2 = othercircle.speed;


              //swap speeds method
              //circle.speed = oldspeed2;
              //othercircle.speed = oldspeed1;
            }

            if (sqrt(pow(circle.pos.x- othercircle.pos.x, 2) + pow(circle.pos.y - othercircle.pos.y, 2)) <= circle.size/2+othercircle.size/2)
            {
              if (circle._color == Global.selectedcolor||  othercircle._color == Global.selectedcolor)
              {

                gameRun = false;
                circle.collided = true;
                othercircle.collided= true;
                circle._color = circle._originalcolor;
                othercircle._color = othercircle._originalcolor;
                //circle._color = Global.selectedcolor;
                //othercircle._color = Global.selectedcolor;
                collidedcircle1 = circle;

                collidedcircle1.posinlist = 1;
                collidedcircle2 = othercircle;
                collidedcircle2.posinlist = 1;
              } else
              {
                PVector oldspeed1 = circle.speed;
                PVector oldspeed2 = othercircle.speed;
                PVector d = new PVector(newpos1.x-newpos2.x, newpos1.y-newpos2.y);
                if (!circle.isstatic)
                {
                  circle.pos.add(d.normalize());
                }
                if (!othercircle.isstatic)
                {

                  othercircle.pos.sub(d.normalize());
                }
                //swap speeds method
                if (othercircle.isstatic && !circle.isstatic)
                {

                  if (circle.pos.x - circle.size/2 < othercircle.pos.x)
                  {
                    circle.speed.x = -abs(circle.speed.x);
                    //othercircle.speed.x = abs(othercircle.speed.x);
                  }
                  if (circle.pos.x + circle.size/2 > othercircle.pos.x)
                  {
                    circle.speed.x = abs(circle.speed.x);
                  }
                  if (circle.pos.y - circle.size/2 < othercircle.pos.y)
                  {
                    circle.speed.y = -abs(circle.speed.y);
                  }
                  if (circle.pos.y + circle.size/2 > othercircle.pos.y)
                  {
                    circle.speed.y = abs(circle.speed.y);
                  }
                }
                if (!circle.isstatic && !othercircle.isstatic)
                {
                  if (circle.linkedcircle != othercircle && circle.linkedcircle != circle)
                  {
                    // circle.linkedcircle.speed = oldspeed2;
                  }


                  circle.speed = oldspeed2;
                  othercircle.speed = oldspeed1;
                }


                // circle.pos.x += circle.speed.x;
                // circle.pos.y += circle.speed.y;
                //othercircle.pos.x +=othercircle.speed.x;
                // othercircle.pos.y += othercircle.speed.y;
                //if(circle.pos.x - circle.size/2 < othercircle.pos.x)
                //{
                //  circle.speed.x = -abs(circle.speed.x);
                //  //othercircle.speed.x = abs(othercircle.speed.x);
                //}
                //if(circle.pos.x + circle.size/2 > othercircle.pos.x)
                //{
                //  circle.speed.x = abs(circle.speed.x);
                //}
                //if(circle.pos.y - circle.size/2 < othercircle.pos.y)
                //{
                //  circle.speed.y = -abs(circle.speed.y);
                //}
                //if(circle.pos.y + circle.size/2 > othercircle.pos.y)
                //{
                //  circle.speed.y = abs(circle.speed.y);
                //}
              }
            }
          }
        }
      }
    }
    if (!gameRun)
    {

      if (keyCode == ' ' && lastKeyCode != ' ')
      {
        Reset();
      }
    }
    if (!gameRun)
    {
      pushMatrix();
      if (alpha < 211)
      {
        alpha+=20;
      }
      noStroke();
      fill(Global.selectedcolor, alpha);
      rect(0, 0, width, height);
      popMatrix();
      pushMatrix();
      noFill();
      stroke(0, alpha/2 + 150);

      strokeWeight(10);
      ellipse(collidedcircle1.pos.x, collidedcircle1.pos.y, collidedcircle1.size, collidedcircle1.size);
      ellipse(collidedcircle2.pos.x, collidedcircle2.pos.y, collidedcircle2.size, collidedcircle2.size);

      popMatrix();
    }
    //if (collidedcircle1 != null && !gameRun)
    //{
    //  collidedcircle1.Draw();
    //}
    //if (collidedcircle2 != null && !gameRun)
    //{
    //  collidedcircle2.Draw();
    //}
    lastKeyCode = keyCode;
  }
}
