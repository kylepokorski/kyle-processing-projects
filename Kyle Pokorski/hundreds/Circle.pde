public class Circle
{
 public PVector pos;
 public PVector speed;
 public color _color;
  public color _originalcolor;
 public float size;
 public float number =0;
 PFont font;
 public boolean shrink = false;
 
 
  public Circle(PVector pos, PVector speed,float size,color _color)
  {
    this.pos = pos;
    this.speed = speed;
    this._color = _color;
    _originalcolor = _color;
    this.size = size;
    font = loadFont("Arial-BoldMT-64.vlw");
  }
  
 public void Update()
 {
   number = (size-70)/6;
   if(shrink)
   {
     if(size > 70 && _color != color(230,0,0))
     {
       size-= 0.2;
     }
   }
   pos.x+=speed.x;
   pos.y += speed.y;
   if(pos.x  + size/2  > width)
   {
     speed.x = -abs(speed.x);
   }
   if(pos.y + size/2 > height)
   {
     speed.y = -abs(speed.y); 
   }
   if(pos.x - size/2 < 0)
   {
     speed.x = abs(speed.x);
   }
   if(pos.y - size/2 < 0)
   {
     speed.y = abs(speed.y);
   }
   
 }
 public void Draw()
 {
   
   pushMatrix();
   noStroke();
   fill(_color);
   ellipse(pos.x,pos.y,size,size);
   if(shrink)
   {
     pushMatrix();
     
     fill(color(235,235,235));
     
       
     ellipse(pos.x,pos.y,2.55*size/3,2.55*size/3);
     fill(_color);
       ellipse(pos.x,pos.y,5*size/8,5*size/8);
     popMatrix();
   }
   pushMatrix();
   float scale = 0.25 + number/60;
     String text = Integer.toString(round(number));
   translate(pos.x - ((textWidth(text))*scale)/2,pos.y +32*scale);
   textFont(font);
   fill(255,255,255);
   scale(scale);
 
  
   text(round(number),0,0 );
   popMatrix();
   popMatrix();
   
 }
}
