PVector pos = new PVector(50, 50);
int flipy = 0;
float speedx=1.5;
float speedy =2.3;
Ball ball;
Ball ball2;
Ball[] balls= new Ball[1000];
boolean regenerate = false;
ArrayList<Ball> newshapes = new ArrayList<Ball>();
void setup()
{
  
  
  noStroke();
  ball = new Ball(new PVector(60, 50), 2.5f, 1.3f, new PVector(30, 30), color(50, 59, 30), 4);
  for (int i = 0; i<balls.length; i++)
  {
    int rand = (int)random(0, 5);
    if (rand == 2)
    {
      rand = (int)(random(2, 4));
    }

    balls[i] = new Ball(new PVector(random(0, 500), random(0, 500)), random(-1.5, 1.5), random(-1.5, 1.5), new PVector(10, 10), color(0, 0, i*255/balls.length), rand);
    // balls[i] = new Ball(new PVector(random(0, 500), random(0, 500)),  1.5 - 0.1*i, 1.5-0.1*i ,new PVector(10, 10), color(0, 0, i*255/balls.length),(int)random(0,2));
    //balls[i] = new Ball(new PVector(60+i, 70+i/2),  1.5 - 0.01*i, 1.5-0.01*i ,new PVector(10, 10), color(0, 0, i*255/balls.length),(int)random(0,3));
  }
}
void settings()
{
   pixelDensity(displayDensity());
  size(520, 520);
 
}
void mouseDragged()
{
  int rand = (int)random(0, 3);
  if (rand == 2)
  {
    rand = (int)(random(2, 4));
  }
  //newshapes.add(new Ball(new PVector(mouseX, mouseY), random(-1.5, 1.5), random(-1.5, 1.5), new PVector(10, 10), color((int)random(0, 255), (int)random(0, 255), (int)random(0, 255)), rand));
  boolean conf = true;

  int type = (int)random(0, 4);
  if (!conf)
  {

    if (type == 0)
    {
      newshapes.add(new Ball(new PVector(mouseX, mouseY), random(-1.5, 1.5), random(-1.5, 1.5), new PVector(10, 10), color((int)random(0, 255), 0, 0), rand));
    }
    if (type == 1)
    {
      newshapes.add(new Ball(new PVector(mouseX, mouseY), random(-1.5, 1.5), random(-1.5, 1.5), new PVector(10, 10), color(0, (int)random(0, 255), 0), rand));
    }
    if (type == 2)
    {
      newshapes.add(new Ball(new PVector(mouseX, mouseY), random(-1.5, 1.5), random(-1.5, 1.5), new PVector(10, 10), color(0, 0, (int)random(0, 255)), rand));
    }
    if (type == 3)
    {
      
      newshapes.add(new Ball(new PVector(mouseX, mouseY), random(-1.5, 1.5), random(-1.5, 1.5), new PVector(10, 10), color(0, (int)random(0, 255), (int)random(0, 255)), rand));
    }
  }
  else
  {
          newshapes.add(new Ball(new PVector(mouseX, mouseY), random(-1.5, 1.5), random(-1.5, 1.5), new PVector(10, 10), color((int)random(0, 255), (int)random(0, 255), (int)random(0, 255)), rand));
  }
}
//void check(float p, float speed, float bottombound)
//{
//  int flip = 0;
//  if (flip == 0 && p +30 <=bottombound)
//  {
//    p +=speed;
//  }
//  if (flip == 1 && p>= 0)
//  {
//    p -=speed;
//  }
//  if (p +30>bottombound && flip == 0 )
//  {
//    flip = 1;
//  }
//  if (p<0 && flip == 1 )
//  {
//    flip = 0;
//  }
//}

void check(float p, float speed, float bottombound)
{

  if (p +30 <=bottombound& p>= 0)
  {
    p +=speed;
  }

  if (p +30>bottombound  )
  {
    speed = -speed;
  }
  if (p<0 )
  {
    speed = -speed;
    //flip = 0;
  }
}
void draw()
{

  if (keyPressed)
  {
    if (key == 'c')
    {
      for (int i = 0; i<newshapes.size(); i++)
      {
        newshapes.remove(i);
      }
    }
    if (key == 'r' && !regenerate)
    {
      for (int i = 0; i<balls.length; i++)
      {
        balls[i] = new Ball(new PVector(random(0, 500), random(0, 500)), 1.5f + random(-1.75, 0.75), 1.5f + + random(-1.75, 0.75), new PVector(20, 20), color(0, 0, i*255/balls.length), (int)(random(0, 2)));
      }
      regenerate = true;
    }
  } else
  {
    regenerate = false;
  }
  for (int i = 0; i< balls.length; i++)
  {
    balls[i].Move();
  }
  if (newshapes.size() > 0)
  {
    for (int i = 0; i<newshapes.size(); i++)
    {
      Ball ball = newshapes.get(i);
      ball.Move();
    }
  }
  ball.Move();
  //check(pos.x, 1.5f, width);
  //check(pos.y, 1.5f, width);

  background(60, 60, 60);
  ball.Draw();
  for (int i = 0; i< balls.length; i++)
  {
    balls[i].Draw();
  }
  if (newshapes.size() > 0)
  {
    for (int i = 0; i<newshapes.size(); i++)
    {
      Ball ball = newshapes.get(i);
      ball.Draw();
    }
  }
}
