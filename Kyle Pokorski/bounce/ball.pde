class Ball
{

  PVector pos;
  float speedx;
  float speedy;
  PVector size;
  color c;
  int type = 1;
  // 0 = rect, 1 = circle
  PVector xbounds;
  PVector ybounds;



  public Ball(PVector pos, float speedx, float speedy, PVector size, color c, int type)
  {
    this.type=type;
    this.pos = pos;
    this.speedx = speedx;
    this.speedy = speedy;
    this.size = size;
    this.c = c;
    //if(type ==2)
    //{
    //  type = (int)random(2,4);
    //}
    if (type == 0||type ==2 || type == 3)
    {
      xbounds = new PVector(0, width);
      ybounds= new PVector(0, height);
    }
    if (type == 1|| type == 4)
    {
      xbounds = new PVector(size.x/2, width+size.x/2);
      ybounds = new PVector(size.y, height+size.y/2);
    }
  }
  public Ball(PVector pos, float speedx, float speedy, PVector size, color c, PVector xbounds, PVector ybounds, int type)
  {
    this.type = type;
    this.pos = pos;
    this.speedx = speedx;
    this.speedy = speedy;
    this.size = size;
    this.c = c;
    this.xbounds = xbounds;
    this.ybounds = ybounds;
  }
  void Move()
  {

    if (pos.x +size.x <= xbounds.y||pos.x>= xbounds.x)
    {
      pos.x +=speedx;
    }

    if (pos.x +size.x>xbounds.y)
    {
      speedx = -speedx;
     pos.x = xbounds.y - size.x;
    }
    if (pos.x<xbounds.x)
    {
      speedx = -speedx;
      pos.x = xbounds.x;
      //flip = 0;
    }

    if (pos.y +size.y <=ybounds.y || pos.y>= ybounds.x)
    {
      pos.y +=speedy;
    }

    if (pos.y +size.y>ybounds.y )
    {
      speedy = -speedy;
      pos.y = ybounds.y - size.y;
    }
    if (pos.y<ybounds.x )
    {
      speedy = -speedy;
      pos.y = ybounds.x;
      //flip = 0;
    }
  }
  void Draw()
  {
    fill(c);
    if (type == 0)
    {
      rect(pos.x, pos.y, size.x, size.y);
    }
    if (type == 1)
    {
      ellipse(pos.x, pos.y, size.x, size.y);
    }
    if (type == 2)
    {
      triangle(pos.x, pos.y, pos.x + size.x, pos.y, pos.x + size.x/2, pos.y + size.y);
    }
    if (type == 3)
    {
      triangle(pos.x, pos.y + size.y, pos.x + size.x, pos.y + size.y, pos.x + size.x/2, pos.y);
    }
    if (type == 4)
    {
      //pushMatrix();

      //translate(pos.x + size.x/2, pos.y + size.y/2);
     
      //rotate(PI/4);
      //rect(pos.x, pos.y, size.x, size.y);
      //popMatrix();
      quad(pos.x,pos.y, pos.x -size.x/2, pos.y + size.y/2, pos.x, pos.y + size.y, pos.x + size.x/2,pos.y + size.y/2);
    }
  }
}
