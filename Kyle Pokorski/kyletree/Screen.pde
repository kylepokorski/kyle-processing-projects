public class Screen
{
  
  protected color backgroundcolor;
  public String screenName;
  public  Screen (color backgroundcolor,String screenName)
  {
    this.backgroundcolor = backgroundcolor;
    this.screenName = screenName;
  }
  public void Draw()
  {
    background(backgroundcolor);
  }
}
