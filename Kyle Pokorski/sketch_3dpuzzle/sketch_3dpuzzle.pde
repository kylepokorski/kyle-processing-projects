int sizex = 3;
int sizey = 3;
int sizez =4;
PVector rotations = new PVector(0, 0, 0);
ArrayList<Integer> values = new ArrayList<Integer>();
void setup()
{
  size(500, 500, P3D);
  for (int i = 0; i<sizex*sizey*sizez; i++)
  {
   // values.add((int)random(0, sizex*sizey*sizez));
      values.add(i);
  }
}
void draw()
{
  background(200, 200, 200);
  if (keyPressed)
  {
    if (keyCode == 'a'||keyCode == 'A')
    {
      rotations.x-= 0.01;
    }
    if (keyCode == 'd'||keyCode == 'D')
    {
      rotations.x +=0.01;
    }
    if (keyCode == 'w' ||keyCode == 'W')
    {
      rotations.y += 0.01f;
    }  
    if (keyCode == 's' ||keyCode == 's')
    {
      rotations.y -= 0.01f;
    }
  }
  pushMatrix();
  translate(200, 200);
  rotateX(rotations.x);
  rotateY(rotations.y);

  for (int i = 0; i<sizex; i++)
  {
    for (int j = 0; j<sizey; j++)
    {
      for (int k = 0; k<sizez; k++)
      {
        pushMatrix();
        translate(30*i, 30*j, 30*k);

        int index = i + sizex*j + sizey*sizex*k;
        stroke(255, 255, 255);
        int value= values.get(i);
        fill(0, 0, value*(255/(sizex*sizey*sizez)));
        box(30, 30, 30);
        popMatrix();
      }
    }
  }
  popMatrix();
}
