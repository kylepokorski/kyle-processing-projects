public class Circle
{
  public PVector pos;
  public PVector speed;
  public color _color;
  public color _originalcolor;
  public float size;
  public float number =0;
  PFont font;
  public boolean shrink = false;
 public int posinlist;
  public float mass = 1;
  public boolean isstatic = false;
  Circle linkedcircle;
  public boolean collided = false;


  public Circle(PVector pos, PVector speed, float size, color _color)
  {
    this.pos = pos;
    this.speed = speed;
    this._color = _color;
    _originalcolor = _color;
    this.size = size;
    font = loadFont("Arial-BoldMT-128.vlw");
    linkedcircle = this;
  }
  public Circle(PVector pos, PVector speed, float size, color _color, Circle linkedcircle)
  {
    this.pos = pos;
    this.speed = speed;
    this._color = _color;
    _originalcolor = _color;
    this.size = size;
    font = loadFont("Arial-BoldMT-128.vlw");
    this.linkedcircle = linkedcircle;
  }

  public void Update()
  {
    number = (size-72)/6.4;
    if (shrink)
    {
      if (size > 70 && _color != color(230, 0, 0))
      {
        size-= 0.2;
      }
    }

    pos.x+=speed.x;
    pos.y += speed.y;
    if (linkedcircle!=this)
    {
      PVector d = new PVector(pos.x - linkedcircle.pos.x, pos.y - linkedcircle.pos.y);
      if (d.mag() > size/2 +linkedcircle.size/2)
      {
        // pos.sub( d.normalize());
        //pos = new PVector(pos.x - d.normalize().x/4,pos.y - d.normalize().y/4);
        //linkedcircle.pos = new PVector(linkedcircle.pos.x + d.normalize().x/4,linkedcircle.pos.y + d.normalize().y/4);
        //linkedcircle.pos.add( d.normalize());
      }

      //if (linkedcircle.pos.x + size/2 > width)
      //{
      //  speed.x = -abs(speed.x);
      //}
      //if (linkedcircle.pos.y + size/2 > height)
      //{
      //  speed.y = -abs(speed.y);
      //}
      //if (linkedcircle.pos.x - size/2 < 0)
      //{
      //  speed.x = abs(speed.x);
      //}
      //if (linkedcircle.pos.y - size/2 < 0)
      //{
      //  speed.y = abs(speed.y);
      //}
    }
    if (pos.x  + size/2  > width)
    {
      speed.x = -abs(speed.x);
      //if (linkedcircle != this)
      //{
      //  linkedcircle.speed.x = -abs(linkedcircle.speed.x);
      //}
    }
    if (pos.y + size/2 > height)
    {
      speed.y = -abs(speed.y);
      //if (linkedcircle != this)
      //{
      //  linkedcircle.speed.y = -abs(linkedcircle.speed.y);
      //}
    }
    if (pos.x - size/2 < 0)
    {
      speed.x = abs(speed.x);
      //if (linkedcircle != this)
      //{
      //  linkedcircle.speed.x = abs(linkedcircle.speed.x);
      //}
    }
    if (pos.y - size/2 < 0)
    {
      speed.y = abs(speed.y);
      //if (linkedcircle != this)
      //{
      //  linkedcircle.speed.y = abs(linkedcircle.speed.y);
      //}
    }
  }
  public void Draw()
  {
    if (this != linkedcircle)
    {
      pushMatrix();
      strokeWeight(3);
      stroke(0, 0, 0);
      PVector d = new PVector(pos.x - linkedcircle.pos.x, pos.y - linkedcircle.pos.y);
      float angle = d.heading();
      line(pos.x, pos.y, linkedcircle.pos.x +(d.normalize().x)*(size/2), linkedcircle.pos.y +(d.normalize().y)*(size/2)); 
      stroke(200, 200, 200);
      strokeWeight(1);
      line(linkedcircle.pos.x, linkedcircle.pos.y, linkedcircle.pos.x + d.normalize().x*(linkedcircle.size)/2, linkedcircle.pos.y + d.normalize().y*(linkedcircle.size)/2);
      popMatrix();
    }
    pushMatrix();
    noStroke();
    fill(_color);
    translate(pos.x, pos.y);
    if (collided)
    {
      stroke(0, 0, 0);
      strokeWeight(7);
    }
    ellipse(0, 0, size, size);
    if (shrink)
    {
      pushMatrix();
      //translate(pos.x, pos.y);
      fill(color(235, 235, 235));
noStroke();

      ellipse(0, 0, 2.6*size/3, 2.6*size/3);
      fill(_color);

      ellipse(0, 0, 19*size/32, 19*size/32);
      popMatrix();
    }

    popMatrix();

    pushMatrix();
    float scale = 0.15 + number/100;
    String text = Integer.toString(round(number));
    if (posinlist != 0)
    {

      translate(pos.x  - (((textWidth(text)*scale/2))), pos.y +60*scale);
    } else
    {
      translate(pos.x - (((textWidth(text)*scale*2))), pos.y +60*scale);
    }
    textFont(font);
    fill(255, 255, 255);
    scale(scale);


    text(round(number), 0, 0 );
    popMatrix();
  }
}
