
void setup()
{
  size(500, 500);
}
void fract(PVector pos, float size, int times)
{
  if (times < 5)
  {
    circle(pos.x,pos.y,size);
    fract(new PVector(pos.x + size/2,pos.y),size/2, times +1);
    fract(new PVector(pos.x ,pos.y+ size/2),size/2, times +1);
    fract(new PVector(pos.x  ,pos.y- size/2),size/2, times +1);
    fract(new PVector(pos.x- size/2  ,pos.y),size/2, times +1);
  }
  else
  {
    
  }
}
void draw()
{
  fract(new PVector(200,200),200,0);
}
