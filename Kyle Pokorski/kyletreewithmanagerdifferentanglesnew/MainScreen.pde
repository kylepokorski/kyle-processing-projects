public class MainScreen extends Screen
{
  public float _lengthdecrease = 0.8f;
  public float _lengthdecreaseright = 0.8f;
  float angleleft = 0;
  float angleright = 0;
  public float _anglechangeleft = 0.01f;
  public float _anglechangeright = 0.01f;
  public boolean threebranches = false;
  public int iterations = 9;
  boolean changewidth = true;
  PFont font;
  public boolean move = true;
  public int displayright = 0;
  public float _width = 0;

  public int _Length = 100;
  public int _LengthRight =100;
  public boolean circlemode = false;
  public int branches = 2;
  boolean differentbranchcolors = false;

  color start =  color(0, 0, 0);
  color end= color(0, 0, 255);
  color rightstart =  color(0, 0, 0);
  color rightend= color(0, 0, 255);
  public int symmetry = 1;
  boolean dosymmetry = true;
 

  public void checklength()
  {
    if (_Length * (1-(float)pow(_lengthdecrease, iterations))/(float)(1-_lengthdecrease) * sin(iterations*angleleft) < 200)
    {
      _Length ++;
    } else
    {
      _Length--;
    }
    if (_LengthRight * (1-pow(_lengthdecrease, iterations))/(1-_lengthdecrease)  < 200)
    {
      _LengthRight ++;
    } else
    {
      _LengthRight--;
    }
  }
  public MainScreen(color backgroundcolor, String screenName)

  {
    super(backgroundcolor, screenName);
    font = loadFont("Arial-BoldMT-20.vlw");
  }

  public void Draw()
  {

    textFont(font);
    super.Draw();
    if (angleleft < PI)
    {
    }
    if (move)
    {
      if (keyPressed)
      {
        if (keyCode == SHIFT)
        {
          angleleft-=0.001f;
          angleright-=0.001f;
        }
      } else
      {
        angleleft+=_anglechangeleft;
          angleright+=_anglechangeright;
       
      }
    }



    stroke(0, 0, 0);
    fill(255, 255, 255);
    strokeWeight(1);
    ellipse(400, 30, 60, 60);
    ellipse(600, 30, 60, 60);
    fill(0, 0, 0);
    rect(595, 10, 5, 30);
    triangle(582, 30, 15 + 582, 30 + 20, 30+582, 30);
    strokeWeight(5);
    ellipse(400, 30, 25, 25);

    pushMatrix();
    translate(400, 30);

    for (int i = 0; i < 16; i++)
    {

      line(0, 0, 0, 20);
      rotate(PI/8);
    }

    popMatrix();
    fill(255, 255, 255);
    ellipse(400, 30, 15, 15);
    fill(0, 0, 0);
    text((angleleft%(2*PI)/PI), 0, 20);
    text("pi rad", 60, 20);
    text((angleleft%(2*PI))/(PI/180), 120, 20);
    text("degrees", 200, 20);
    text((angleright%(2*PI)/PI), 0, 40);
    text("pi rad", 60, 40);
    text((angleright%(2*PI))/(PI/180), 120, 40);
    text("degrees", 200, 40);







    if (!dosymmetry)
    {

      generate(new PVector(350, 500), _Length, _LengthRight, PI/2, angleleft, angleright, 0, _width, _lengthdecrease, _lengthdecreaseright, 0);
    } else
    {
      for (int i = 0; i<symmetry; i++)
      {
        generate(new PVector(350, 500), _Length, _LengthRight, 2*(float)i* (PI/(float)symmetry) + PI/2, angleleft, angleright, 0, _width, _lengthdecrease, _lengthdecreaseright, 0);
      }
    }
  }
  void generate(PVector point, float _length, float _lengthright, float angle, float anglechangeleft, float anglechangeright, int times, float strokewidth, float lengthdecreaseleft, float lengthdecreaseright, int branchnumber)
  {
    if (times <iterations)
    {

      strokeWeight(strokewidth);
      //stroke(0, 0, times *(255/iterations));
      color _color;
      color _rightcolor;
      _color = lerpColor(start, end, (float)times/(float)iterations);
      _rightcolor = lerpColor(rightstart, rightend, (float)times/(float)iterations);
      float newx;
      float newy;
      if (branchnumber == 0)
      {
        stroke(_color);
        newx = point.x + cos(angle)*_length ;
        newy = point.y - _length *sin(angle);
      } else
      {
        stroke(_rightcolor);
        newx = point.x + cos(angle)*_length ;
        newy = point.y - _length *sin(angle);
      }

      if (circlemode)
      {
        if (strokewidth <1)
        {
          strokewidth = 1;
        }



        ellipse(point.x, point.y, strokewidth, strokewidth);
      } else
      {
        line(point.x, point.y, newx, newy);
      }

      float newwidthleft;
      float newwidthright;
      if (changewidth)
      {
        newwidthleft = (float)(lengthdecreaseleft*strokewidth);
        newwidthright = (float)(lengthdecreaseright*strokewidth);
      } else
      {
        newwidthright = strokewidth;
        newwidthleft = strokewidth;
      }
      generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 0);
      if (times >= displayright && branches >= 2)
      {
        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 1);
      }
      if (branches == 3 ||branches == 5 || branches == 7||branches == 9)
      {
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 2);
      }
      if (branches == 4||branches == 5)
      {
        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + PI/2, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 3);
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft -PI/2, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 4);
      }

      if (branches == 6 || branches == 7)
      {
        //  generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + PI/3, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 3);
        //  generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + PI/6, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 4);

        //  generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - PI/3, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 5);
        //  generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - PI/6, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 6);
        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + 2*PI/(branches), anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 3);
        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + 4*PI/(branches), anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 4);

        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - 2*PI/(branches), anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 5);
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - 4*PI/(branches), anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 6);
      }
      if (branches == 8||branches == 9)
      {
        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + PI/2, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 3);
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft -PI/2, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 4);

        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright +  PI, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 3);
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - PI, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 4);

        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright +  3*PI/2, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright, 5);
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - 3*PI/2, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright, 6);
      }
    }
  }
}
