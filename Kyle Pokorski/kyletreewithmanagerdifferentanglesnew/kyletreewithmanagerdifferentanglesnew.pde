//public static ScreenManager manager;
MainScreen main;
SettingsScreen settings;
PFont font;
boolean smooth = true;
void setup()
{
  
  Global.manager = new ScreenManager();
  main = new MainScreen(color(190, 190, 190), "Main");
  settings = new SettingsScreen(main);
  Global.manager.Add(main);
  Global.manager.Add(settings);
  Global.manager.screentype = "Main";
  font = loadFont("Arial-BoldMT-20.vlw");
  noSmooth();
  
}
void draw()
{
  
  Global.manager.Draw();
  
}
void settings()
{
  size(750, 750);
  if(!smooth)
  {
    noSmooth(); //<>//
  }
  else
  {
  smooth();
  
  }
}
void mouseDragged()
{

  //if (Global.manager.screentype == "Main")
  //{
  //  main.angleleft+=0.01;
  //}
}
void mouseClicked()
{

  if (Global.manager.screentype == "Main")
  {
    main.move = !main.move;
  }
  if (sqrt(pow(mouseX - 400, 2) + pow(mouseY, 2)) < 60 && Global.manager.screentype == "Main")
  {
    Global.manager.screentype = "Settings";
  }
  if(sqrt(sq(mouseX-600) + sq(mouseY)) < 60 && Global.manager.screentype == "Main")
  {
    save("tree.png");
  }
  if ( mouseX >= 10 && mouseY >= 570 && mouseX <= 90 && mouseY <=600 && Global.manager.screentype == "Settings")
  {
    Global.manager.screentype = "Main";
  }
  if (Global.manager.screentype == "Settings")
  {
    if (mouseX >=475 && mouseX <= 525 && mouseY >= 10 && mouseY <= 60)
    {
      if (main._lengthdecreaseright > 0.2f)
      {
        main._lengthdecreaseright -=0.01f;
      }
    }
    if (mouseX >=565 && mouseX <= 590 && mouseY >= 10 && mouseY <= 60)
    {
      if (main._lengthdecreaseright <1.15f)
      {
        main._lengthdecreaseright +=0.01f;
      }
    }
    if (mouseX >=330 && mouseX <= 355 && mouseY >= 10 && mouseY <= 60 )
    {
      if (main._lengthdecrease < 1.15f)
      {
        main._lengthdecrease += 0.01;
      }
    }
    if (mouseX>= 245 && mouseX<= 270 && mouseY>=10 && mouseY <= 60)
    {
      if (main._lengthdecrease > 0.2f)
      {
        main._lengthdecrease -=0.01f;
      }
    }
    if (mouseX >= 330 && mouseX <= 355 && mouseY >= 110 && mouseY <= 160)
    {
      if (main.iterations < 19)
      {
        main.iterations ++;
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 110 && mouseY <=160)
    {
      if (main.iterations > 2)
      {
        main.iterations--;
      }
    }
    if (mouseY >= 80 && mouseY <= 100 && mouseX >=250 && mouseX <=290)
    {
      main.circlemode = true;
    }
    if (mouseY >= 80 && mouseY <= 100 && mouseX>= 350 && mouseX <=395)
    {
      main.circlemode = false;
    }
    if (mouseY >= 180 && mouseY <= 200 && mouseX >=250 && mouseX <=290)
    {
      main.changewidth = true;
    }
    if (mouseY >= 180 && mouseY <= 210 && mouseX>= 350 && mouseX <=395)
    {
      main.changewidth = false;
    }
    if (mouseX >= 330 && mouseX <= 355 && mouseY >= 210 && mouseY <= 260)
    {
      if (main._width < 20)
      {
        main._width ++;
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 210 && mouseY <=260)
    {
      if (main._width > 0)
      {
        main._width--;
      }
    }
    if (mouseX >= 330 && mouseX <= 355 && mouseY >= 260 && mouseY <= 310)
    {
      if (main._Length < 130)
      {
        main._Length ++;
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 260 && mouseY <=310)
    {
      if (main._Length > 10)
      {
        main._Length--;
      }
    }
    if (mouseX >=475 && mouseX <= 525 && mouseY >= 260 && mouseY <= 310)
    {
      if (main._LengthRight > 10)
      {
        main._LengthRight --;
      }
    }
    if (mouseX >=565 && mouseX <= 590 && mouseY >= 260 && mouseY <= 310)
    {
      if (main._LengthRight <140)
      {
        main._LengthRight ++;
      }
    }
      if (mouseX >= 330 && mouseX <= 355 && mouseY >= 310 && mouseY <= 360)
    {
      if (main._anglechangeleft < 0.05f)
      {
        main._anglechangeleft +=0.005f;
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 310 && mouseY <=360)
    {
      if (main._anglechangeleft >-0.005)
      {
        main._anglechangeleft-=0.005f;
      }
    }
    if (mouseX >=475 && mouseX <= 525 && mouseY >= 310 && mouseY <= 360)
    {
      if (main._anglechangeright > -0.005)
      {
        main._anglechangeright -=0.005;
      }
    }
    if (mouseX >=565 && mouseX <= 590 && mouseY >= 310 && mouseY <= 360)
    {
      if (main._anglechangeright <0.05f)
      {
        main._anglechangeright +=0.005;
      }
    }
      if (mouseX >= 330 && mouseX <= 355 && mouseY >= 360 && mouseY <= 410)
    {
      if (main.branches < 9)
      {
        main.branches ++;
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 360 && mouseY <=410)
    {
      if (main.branches > 1)
      {
        main.branches--;
      }
    }
     if (mouseX >= 330 && mouseX <= 355 && mouseY >= 410 && mouseY <= 450)
    {
      if (main.symmetry < 20)
      {
        main.symmetry++;
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 410 && mouseY <=450)
    {
      if (main.symmetry > 1)
      {
        main.symmetry--;
      }
    }
  }
}
