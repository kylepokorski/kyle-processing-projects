ArrayList<Circle> circles = new ArrayList<Circle>();
boolean gameRun = true;
float sum;
PFont font;
int lastKeyCode;
RectangleObstacle rect;
Circle collidedcircle1;
Circle collidedcircle2;
float alpha = 0;
void Reset()
{
  alpha = 0;
  //int seed = round(random(0,round(pow(2,32))));
  // randomSeed(seed);
  gameRun = true;
  circles.clear();
  //circles.add(new Circle(new PVector(150, 150), new PVector(1.5f, -1.8f), 70, color(100, 100, 100)));

  //circles.add(new Circle(new PVector(100, 100), new PVector(0,0), 70, color(50, 50, 50)));
  //Circle circlesta = circles.get(0);
  //circlesta.isstatic = true;

 // circles.add(new Circle(new PVector(130, 200), new PVector(-2.5f, -1.8f), 70, color(100, 100, 100)));

  //circles.add(new Circle(new PVector(190, 400), new PVector(1.5f, 1f), 70, color(100, 100, 100), circles.get(0))); 


  for (Integer i = 0; i<40; i++)
  {

    float channel = random(0, 195);
    PVector pos = new PVector(random(70/2, 960 - 70/2), random(70/2, 720-70/2));
    PVector speed = new PVector(random(-1, 1)*3.9f, random(-1, 1)*3.9f);

    for (int j  = 0; j<circles.size(); j++)
    {
      Circle circle = circles.get(j);

      if (sqrt(pow(pos.x -circle.pos.x, 2) + pow(pos.y-circle.pos.y, 2)) < circle.size/2+ 70/2)
      {
        pos = new PVector(random(70/2, 960 - 70/2), random(70/2, 720-70/2));
        j = -1;
      }
    }
    while (-0.2<speed.x   && speed.x<0.2)
    {
      speed.x = random(random(-1, 1)*3.9f);
    }
    while (-0.2<speed.y   && speed.y<0.2)
    {
      speed.y = random(random(-1, 1)*1.9f);
    }
    circles.add(new Circle(pos, speed, 70, color(channel, channel, channel)));
    Circle lastcircle = circles.get(i);
    lastcircle.posinlist = i;
  }
  // rect = new RectangleObstacle(new PVector(100, 100), 50, 150, circles);
}
void setup()
{
  pixelDensity(2);
  size(960, 720);
  Reset();

  font = loadFont("Arial-BoldMT-32.vlw");
}
void draw()
{

  background(235, 235, 235);
  
  fill(0, 0, 0);
  noStroke();
  textFont(font);
  text(round(sum), 0, 32);
  sum = 0;
  // rect.Update();
  //rect.Draw();
  
 
  for (Circle circle : circles)
  {


    sum += circle.number;
    circle.Draw();
    if (gameRun)
    {

      circle.Update();

      if (mousePressed && !circle.isstatic)
      {
        if (sqrt(pow(mouseX - circle.pos.x, 2)+pow((mouseY -circle.pos.y), 2)) < circle.size/2)
        {

          if (circle.number < 100)
          {

            float newsize = circle.size+3;
            if (circle.pos.x - (newsize/2) > 0 && circle.pos.y - (newsize/2) > 0 && circle.pos.x + (newsize/2) <width && circle.pos.y + (newsize/2) < height)
            {
              circle._color = color(230, 0, 0);
              circle.size+=3;
              if (circle.linkedcircle != circle)
              {
                circle.linkedcircle._color = color(230, 0, 0);
                circle.linkedcircle.size+=3;
              }
            }
            if (circle.pos.x - (newsize/2) < 0)
            {
              circle.speed.x +=0.01f;
            }
            if (circle.pos.y - (newsize/2) < 0)
            {
              circle.speed.y +=0.01f;
            }
            if (circle.pos.x + (newsize/2) > 600)
            {
              circle.speed.x -=0.01f;
            }
            if (circle.pos.y + (newsize/2) > 600)
            {
              circle.speed.y -=0.01f;
            }
          }
        } else
        {
          circle._color = circle._originalcolor;
        }
      } else
      {
        if (circle.linkedcircle != circle && circle.linkedcircle._color == color(230, 0, 0))
        {
        } else
        {
          circle._color = circle._originalcolor;
        }
      }

      if (mousePressed && !circle.linkedcircle.isstatic && circle != circle.linkedcircle)
      {
        if (sqrt(pow(mouseX - circle.linkedcircle.pos.x, 2)+pow((mouseY -circle.linkedcircle.pos.y), 2)) < circle.linkedcircle.size/2)
        {

          if (circle.linkedcircle.number < 100)
          {

            float newsize = circle.linkedcircle.size+3;
            if (circle.linkedcircle.pos.x - (newsize/2) > 0 && circle.linkedcircle.pos.y - (newsize/2) > 0 && circle.linkedcircle.pos.x + (newsize/2) <width && circle.linkedcircle.pos.y + (newsize/2) < height)
            {
              circle.linkedcircle._color = color(230, 0, 0);
              //circle.linkedcircle.size+=3;
              if (circle.linkedcircle != circle)
              {
                circle._color = color(230, 0, 0);
                circle.size+=3;
              }
            }
            if (circle.linkedcircle.pos.x - (newsize/2) < 0)
            {
              circle.linkedcircle.speed.x +=0.01f;
            }
            if (circle.linkedcircle.pos.y - (newsize/2) < 0)
            {
              circle.linkedcircle.speed.y +=0.01f;
            }
            if (circle.linkedcircle.pos.x + (newsize/2) > 600)
            {
              circle.linkedcircle.speed.x -=0.01f;
            }
            if (circle.linkedcircle.pos.y + (newsize/2) > 600)
            {
              circle.linkedcircle.speed.y -=0.01f;
            }
          }
        } else
        {
          //circle.linkedcircle._color = circle.linkedcircle._originalcolor;
        }
      } else
      {
        //if(circle != circle.linkedcircle)
        //{
        //circle.linkedcircle._color = circle.linkedcircle._originalcolor;
        //}
      }


      for (Circle othercircle : circles)
      {
        if (othercircle != circle)
        {

          PVector newpos1 = new PVector(circle.pos.x+circle.speed.x, circle.pos.y+circle.speed.y);
          PVector newpos2 = new PVector(othercircle.pos.x+othercircle.speed.x, othercircle.pos.y+othercircle.speed.y);

          if (sqrt(pow(newpos1.x - newpos2.x, 2) + pow(newpos1.y-newpos2.y, 2)) <= circle.size/2 + othercircle.size/2)
          {


            PVector oldspeed1 = circle.speed;
            PVector oldspeed2 = othercircle.speed;


            //swap speeds method
            //circle.speed = oldspeed2;
            //othercircle.speed = oldspeed1;
          }

          if (sqrt(pow(circle.pos.x- othercircle.pos.x, 2) + pow(circle.pos.y - othercircle.pos.y, 2)) <= circle.size/2+othercircle.size/2)
          {
            if (circle._color == color(230, 0, 0)||  othercircle._color == color(230,0,0))
            {

              gameRun = false;
              circle.collided = true;
              othercircle.collided= true;
              collidedcircle1 = circle;
              collidedcircle1.posinlist = 1;
              collidedcircle2 = othercircle;
              collidedcircle2.posinlist = 1;
            } else
            {
              PVector oldspeed1 = circle.speed;
              PVector oldspeed2 = othercircle.speed;
              PVector d = new PVector(newpos1.x-newpos2.x, newpos1.y-newpos2.y);
              circle.pos.add(d.normalize());
              othercircle.pos.sub(d.normalize());
              PVector distanceVect = PVector.sub(othercircle.pos, circle.pos);

              // Calculate magnitude of the vector separating the balls
              float distanceVectMag = distanceVect.mag();

              // Minimum distance before they are touching
              float minDistance = circle.size/2 + othercircle.size/2;
              float distanceCorrection = (minDistance-distanceVectMag)/2.0;
              PVector _d = distanceVect.copy();
              PVector correctionVector = _d.normalize().mult(distanceCorrection);
              othercircle.pos.add(correctionVector);
              circle.pos.sub(correctionVector);

              // get angle of distanceVect
              float theta  = distanceVect.heading();
              // precalculate trig values
              float sine = sin(theta);
              float cosine = cos(theta);

              /* bTemp will hold rotated ball positions. You 
               just need to worry about bTemp[1] position*/
              PVector[] bTemp = {
                new PVector(), new PVector()
              };

              /* this ball's position is relative to the other
               so you can use the vector between them (bVect) as the 
               reference point in the rotation expressions.
               bTemp[0].position.x and bTemp[0].position.y will initialize
               automatically to 0.0, which is what you want
               since b[1] will rotate around b[0] */
              bTemp[1].x  = cosine * distanceVect.x + sine * distanceVect.y;
              bTemp[1].y  = cosine * distanceVect.y - sine * distanceVect.x;

              // rotate Temporary velocities
              PVector[] vTemp = {
                new PVector(), new PVector()
              };

              vTemp[0].x  = cosine * circle.speed.x + sine * circle.speed.y;
              vTemp[0].y  = cosine * circle.speed.y - sine * circle.speed.x;
              vTemp[1].x  = cosine * othercircle.speed.x + sine * othercircle.speed.y;
              vTemp[1].y  = cosine * othercircle.speed.y - sine * othercircle.speed.x;

              /* Now that velocities are rotated, you can use 1D
               conservation of momentum equations to calculate 
               the final velocity along the x-axis. */
              PVector[] vFinal = {  
                new PVector(), new PVector()
              };

              // final rotated velocity for b[0]
              vFinal[0].x = ((circle.mass - othercircle.mass) * vTemp[0].x + 2 * othercircle.mass * vTemp[1].x) / (circle.mass + othercircle.mass);
              vFinal[0].y = vTemp[0].y;

              // final rotated velocity for b[0]
              vFinal[1].x = ((othercircle.mass - circle.mass) * vTemp[1].x + 2 * circle.mass * vTemp[0].x) / (circle.mass + othercircle.mass);
              vFinal[1].y = vTemp[1].y;

              // hack to avoid clumping
              bTemp[0].x += vFinal[0].x;
              bTemp[1].x += vFinal[1].x;

              /* Rotate ball positions and velocities back
               Reverse signs in trig expressions to rotate 
               in the opposite direction */
              // rotate balls
              PVector[] bFinal = { 
                new PVector(), new PVector()
              };

              bFinal[0].x = cosine * bTemp[0].x - sine * bTemp[0].y;
              bFinal[0].y = cosine * bTemp[0].y + sine * bTemp[0].x;
              bFinal[1].x = cosine * bTemp[1].x - sine * bTemp[1].y;
              bFinal[1].y = cosine * bTemp[1].y + sine * bTemp[1].x;

              // update balls to screen position
              othercircle.pos.x = circle.pos.x + bFinal[1].x;
              othercircle.pos.y = circle.pos.y + bFinal[1].y;

              circle.pos.add(bFinal[0]);

              // update velocities
              if (!circle.isstatic)
              {
                circle.speed.x = cosine * vFinal[0].x - sine * vFinal[0].y;
                circle.speed.y = cosine * vFinal[0].y + sine * vFinal[0].x;
              }
              if (!othercircle.isstatic)
              {

                othercircle.speed.x = cosine * vFinal[1].x - sine * vFinal[1].y;
                othercircle.speed.y = cosine * vFinal[1].y + sine * vFinal[1].x;
              }
              //swap speeds method
              //if (othercircle.isstatic && !circle.isstatic)
              //{

              //  if (circle.pos.x - circle.size/2 < othercircle.pos.x)
              //  {
              //    circle.speed.x = -abs(circle.speed.x);
              //    //othercircle.speed.x = abs(othercircle.speed.x);
              //  }
              //  if (circle.pos.x + circle.size/2 > othercircle.pos.x)
              //  {
              //    circle.speed.x = abs(circle.speed.x);
              //  }
              //  if (circle.pos.y - circle.size/2 < othercircle.pos.y)
              //  {
              //    circle.speed.y = -abs(circle.speed.y);
              //  }
              //  if (circle.pos.y + circle.size/2 > othercircle.pos.y)
              //  {
              //    circle.speed.y = abs(circle.speed.y);
              //  }
              //}
              //if (!circle.isstatic && !othercircle.isstatic)
              //{
              //  if (circle.linkedcircle != othercircle && circle.linkedcircle != circle)
              //  {
              //    // circle.linkedcircle.speed = oldspeed2;
              //  }


              //  circle.speed = oldspeed2;
              //  othercircle.speed = oldspeed1;
              //}


              // circle.pos.x += circle.speed.x;
              // circle.pos.y += circle.speed.y;
              //othercircle.pos.x +=othercircle.speed.x;
              // othercircle.pos.y += othercircle.speed.y;
              //if(circle.pos.x - circle.size/2 < othercircle.pos.x)
              //{
              //  circle.speed.x = -abs(circle.speed.x);
              //  //othercircle.speed.x = abs(othercircle.speed.x);
              //}
              //if(circle.pos.x + circle.size/2 > othercircle.pos.x)
              //{
              //  circle.speed.x = abs(circle.speed.x);
              //}
              //if(circle.pos.y - circle.size/2 < othercircle.pos.y)
              //{
              //  circle.speed.y = -abs(circle.speed.y);
              //}
              //if(circle.pos.y + circle.size/2 > othercircle.pos.y)
              //{
              //  circle.speed.y = abs(circle.speed.y);
              //}
            }
          }
        }
      }
    }
  }
  if (!gameRun)
  {

    if (keyCode == ' ' && lastKeyCode != ' ')
    {
      Reset();
    }
  }
   if(!gameRun)
  {
    pushMatrix();
    if(alpha < 210)
    {
      alpha+=20;
    }
    noStroke();
    fill(230,0,0,alpha);
    rect(0,0,width,height);
    popMatrix();
  }
  if(collidedcircle1 != null && !gameRun)
  {
    collidedcircle1.Draw();
  }
  if(collidedcircle2 != null && !gameRun)
  {
    collidedcircle2.Draw();
  }
  lastKeyCode = keyCode;
}
//void mouseDragged()
//{
//  for(Circle circle: circles)
//  {
//    if(sqrt(pow(mouseX - circle.pos.x,2)+pow((mouseY -circle.pos.x),2)) < circle.size)
//    {

//      if(circle.number < 100)
//      {

//        if(circle.pos.x - ((circle.size+5)/2) > 0)
//        {
//          circle._color = color(255,0,0);
//      circle.size+=5;
//        }
//      }
//    }

//  }
//}
