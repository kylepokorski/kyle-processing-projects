class SettingsScreen extends Screen
{
  MainScreen main;
  PFont font;
  public SettingsScreen(MainScreen main)
  {
    super(color(128, 128, 128), "Settings");
    this.main = main;
  }
  void Draw()
  {
    super.Draw();
    font = loadFont("Arial-BoldMT-20.vlw");
    stroke(0, 0, 0);
    fill(0, 0, 0);
    textFont(font);
    text("1. Length Decrease Left", 10, 50);
    text("Right",400,50);
    text (main._lengthdecrease, 270, 50);
    text(main._lengthdecreaseright,500,50);
    text("2. Circle Mode:", 10, 100);
    text("3. Iterations", 10, 150);
    text(main.iterations, 290, 150);
    text("4. Reduce Width",10,200);
    text("5. Width:",10,250);
    text(main._width,290,250);
    text("6. Length",10,300);
    text(main._Length,270,300);
    text(main._LengthRight,520,300);
    text("7. Angle Change",10,350);
    text(main._anglechangeleft,270,350);
    text(main._anglechangeright,500,350);
    text("8. Branches",10,400);
    text(main.branches,290,400);
    text("Resume", 10, 500);
    
    
    
    if (main.circlemode)
    {
      fill(255, 0, 0);
      text("true", 250, 100);
      fill(0, 0, 0);
      text("false", 350, 100);
    } else
    {
      fill(255, 0, 0);
      text("false", 350, 100);

      fill(0, 0, 0);
      text("true", 250, 100);
    }
    
      if (main.changewidth)
    {
      fill(255, 0, 0);
      text("true", 250, 200);
      fill(0, 0, 0);
      text("false", 350, 200);
    } else
    {
      fill(255, 0, 0);
      text("false", 350, 200);

      fill(0, 0, 0);
      text("true", 250, 200);
    }
    


    noStroke();
    fill(255, 0, 0);
    triangle(330, 10, 355, 35, 330, 60);
    triangle(270, 10, 245, 35, 270, 60);
    triangle(500, 10, 475, 35, 500, 60);
    triangle(565,10,590,35,565,60);
    triangle(330, 10+ 100, 355, 35+100, 330, 60 + 100);
    triangle(270, 10+100, 245, 35+100, 270, 60 + 100);
    triangle(330, 10+ 200, 355, 35+200, 330, 60 + 200);
    triangle(270, 10+200, 245, 35+200, 270, 60 + 200);
    triangle(330, 10+ 250, 355, 35+250, 330, 60 + 250);
    triangle(270, 10+250, 245, 35+250, 270, 60 + 250);
    triangle(500, 10+250, 475, 35+250, 500, 60+250);
    triangle(565,10+250,590,35+250,565,60+250);
    triangle(500, 10+300, 475, 35+300, 500, 60+300);
    triangle(565,10+300,590,35+300,565,60+300);
     triangle(330, 10+ 300, 355, 35+300, 330, 60 + 300);
    triangle(270, 10+300, 245, 35+300, 270, 60 + 300);
    triangle(330, 10+ 350, 355, 35+350, 330, 60 + 350);
    triangle(270, 10+350, 245, 35+350, 270, 60 + 350);
  }
}
