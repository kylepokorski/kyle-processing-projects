public class MainScreen extends Screen
{
  public float _lengthdecrease = 0.9f;
  public float _lengthdecreaseright = 0.5f;
  float angleleft = 0;
  float angleright = 0;
  public float _anglechangeleft = 0.01f;
  public float _anglechangeright = 0.01f;
  public boolean threebranches = false;
  public int iterations = 6;
  boolean changewidth = false;
  PFont font;
  public boolean move = true;
  public int displayright = 0;
  public int _width = 10;

  public int _Length = 75;
  public int _LengthRight = 75;
  public boolean circlemode = false;
  public int branches = 4;
  color start =  color(0,0, 0);
  color end= color(0, 0, 255);
  public MainScreen(color backgroundcolor, String screenName)

  {
    super(backgroundcolor, screenName);
    font = loadFont("Arial-BoldMT-20.vlw");
  }

  public void Draw()
  {

    textFont(font);
    super.Draw();
    if (angleleft < PI)
    {
    }
    if (move)
    {
      angleleft+=_anglechangeleft;
      angleright+=_anglechangeright;
    }



    stroke(0, 0, 0);
    fill(255, 255, 255);
    strokeWeight(1);
    ellipse(400, 30, 60, 60);

    fill(0, 0, 0);
    strokeWeight(5);
    ellipse(400, 30, 25, 25);
    pushMatrix();
    translate(400, 30);

    for (int i = 0; i < 16; i++)
    {

      line(0, 0, 0, 20);
      rotate(PI/8);
    }

    popMatrix();
    fill(255, 255, 255);
    ellipse(400, 30, 15, 15);
    fill(0, 0, 0);
    text((angleleft%(2*PI)), 0, 20);
    text("rad", 60, 20);
    text((angleright%(2*PI))/(PI/180), 120, 20);
    text("degrees", 200, 20);
    text((angleleft%(2*PI)), 0, 40);
    text("rad", 60, 40);
    text((angleright%(2*PI))/(PI/180), 120, 40);
    text("degrees", 200, 40);







    generate(new PVector(250, 400), _Length, _LengthRight, PI/2, angleleft, angleright, 0, _width, _lengthdecrease, _lengthdecreaseright);
  }
  void generate(PVector point, float _length, float _lengthright, float angle, float anglechangeleft, float anglechangeright, int times, int strokewidth, float lengthdecreaseleft, float lengthdecreaseright)
  {
    if (times <iterations)
    {

      strokeWeight(strokewidth);
      //stroke(0, 0, times *(255/iterations));
      stroke(lerpColor(start, end, (float)times/(float)iterations));
      float newx = point.x + cos(angle)*_length ;
      float newy = point.y - _length *sin(angle);
      if (circlemode)
      {
        if (strokewidth <1)
        {
          strokewidth = 1;
        }
        ellipse(point.x, point.y, strokewidth, strokewidth);
      } else
      {
        line(point.x, point.y, newx, newy);
      }

      int newwidthleft;
      int newwidthright;
      if (changewidth)
      {
        newwidthleft = (int)(lengthdecreaseleft*strokewidth);
        newwidthright = (int)(lengthdecreaseright*strokewidth);
      } else
      {
        newwidthright = strokewidth;
        newwidthleft = strokewidth;
      }
      generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright);
      if (times >= displayright)
      {
        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright);
      }
      if (branches == 3 ||branches == 5 || branches == 7)
      {
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright);
      }
      if (branches == 4||branches == 5 || branches == 8)
      {
        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + PI/2, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright);
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - PI/2, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright);
      }
      if (branches == 6 || branches == 7)
      {
        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + PI/3, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright);
        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright + PI/6, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright);

        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - PI/3, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright);
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - PI/6, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright);
      }
      if (branches == 8)
      {
        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright +  PI/4, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright);
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - PI/4, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright);

        generate(new PVector(newx, newy), _lengthright*lengthdecreaseright, _length*lengthdecreaseleft, angle + anglechangeright +  3*PI/4, anglechangeright, anglechangeright, times+1, newwidthright, lengthdecreaseleft, lengthdecreaseright);
        generate(new PVector(newx, newy), _length*lengthdecreaseleft, _lengthright*lengthdecreaseright, angle - anglechangeleft - 3*PI/4, anglechangeleft, anglechangeright, times+1, newwidthleft, lengthdecreaseleft, lengthdecreaseright);
      }
    }
  }
}
