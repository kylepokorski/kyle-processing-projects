public class Circle
{
  public PVector pos;
  public PVector speed;
  public color _color;
  public color _originalcolor;
  public float size;
  public float number =0;
  PFont font;
  public boolean shrink = false;
  int posinlist;
  public float mass = 1;
  boolean changemass = true;


  public Circle(PVector pos, PVector speed, float size, color _color)
  {
    this.pos = pos;
    this.speed = speed;
    this._color = _color;
    _originalcolor = _color;
    this.size = size;
    font = loadFont("Arial-BoldMT-128.vlw");
  }

  public void Update()
  {
    number = (size-70)/6;
    if (shrink)
    {
      if (size > 70 && _color != color(230, 0, 0))
      {
        size-= 0.2;
      }
    }
    if(changemass)
    {
      mass = number/100 +1;
    }
    pos.x+=speed.x;
    pos.y += speed.y;
    if (pos.x  + size/2  > width)
    {
      speed.x = -abs(speed.x);
    }
    if (pos.y + size/2 > height)
    {
      speed.y = -abs(speed.y);
    }
    if (pos.x - size/2 < 0)
    {
      speed.x = abs(speed.x);
    }
    if (pos.y - size/2 < 0)
    {
      speed.y = abs(speed.y);
    }
  }
  public void Draw()
  {

    pushMatrix();
    noStroke();
    fill(_color);
    translate(pos.x, pos.y);
    ellipse(0, 0, size, size);
    if (shrink)
    {
      pushMatrix();
      //translate(pos.x, pos.y);
      fill(color(235, 235, 235));


      ellipse(0, 0, 2.55*size/3, 2.55*size/3);
      fill(_color);
      ellipse(0, 0, 5*size/8, 5*size/8);
      popMatrix();
    }

    popMatrix();

    pushMatrix();
    float scale = 0.15 + number/100;
    String text = Integer.toString(round(number));
    if (posinlist != 0)
    {

      translate(pos.x  - (((textWidth(text)*scale/2))), pos.y +64*scale);
    } else
    {
      translate(pos.x - (((textWidth(text)*scale*2))), pos.y +64*scale);
    }
    textFont(font);
    fill(255, 255, 255);
    scale(scale);


    text(round(number), 0, 0 );
    popMatrix();
  }
}
