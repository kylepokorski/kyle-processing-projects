public class RectangleObstacle
{
  float _width;
  float _height;
  PVector pos;
  ArrayList<Circle> circles;
  
  public RectangleObstacle(PVector pos, float _width,float _height,ArrayList<Circle> circles)
  {
    this.pos = pos;
    this._width = _width;
    this._height = _height;
    this.circles = circles;
  }
  public void Update()
  {
    for(Circle circle : circles)
    {
      if(circle.pos.x - circle.size/2 < pos.x + _width && circle.pos.y - circle.size/2 <pos.y + _height && circle.pos.y -circle.size/2 > pos.y)
      {
        if(circle._color == color(230,230,230))
        {
          gameRun = false;
        }
       circle.speed.x = abs( circle.speed.x);
      }
      //if(circle.pos.x+ circle.size/2< pos.x+ _width && circle.pos.y - circle.size/2 <pos.y + _height && circle.pos.y -circle.size/2 > pos.y)
      //{
      //  circle.speed.x = -abs(circle.speed.x);
      //}
      if(circle.pos.y - circle.size/2 < pos.y +_height && circle.pos.x - circle.size/2 < pos.x + _width && circle.pos.x > pos.x)
      {
         if(circle._color == color(230,230,230))
        {
          gameRun = false;
        }
        circle.speed.y = abs(circle.speed.y);
      }
      
      
    }
  }
  public void Draw()
  {
    noStroke();
    fill(0,0,0);
    rect(pos.x,pos.y,_width,_height);
  }
}
