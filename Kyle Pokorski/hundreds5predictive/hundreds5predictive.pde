ArrayList<Circle> circles = new ArrayList<Circle>();
boolean gameRun = true;
float sum;
PFont font;
int lastKeyCode;
void Reset()
{
  //int seed = round(random(0,round(pow(2,32))));
  // randomSeed(seed);
  gameRun = true;
  circles.clear();
  //circles.add(new Circle(new PVector(150, 150), new PVector(1.5f, -1.8f), 70, color(100, 100, 100)));

  //circles.add(new Circle(new PVector(100, 100), new PVector(-1.6f, -1.5f), 70, color(50, 50, 50)));
  //circles.add(new Circle(new PVector(200,200),new PVector(0,0),100,color(0,0,0)));
  //Circle staticcircle = circles.get(0);
  //staticcircle.posinlist = 0;
  //staticcircle.isstatic = true;
  for (Integer i = 0; i< 80; i++)
  {

    float channel = random(0, 195);
    PVector pos = new PVector(random(70/2, 960 - 70/2), random(70/2, 720-70/2));
    PVector speed = new PVector(random(-1, 1)*1.9f, random(-1, 1)*1.9f);

    for (int j  = 0; j<circles.size(); j++)
    {
      Circle circle = circles.get(j);

      if (sqrt(pow(pos.x -circle.pos.x, 2) + pow(pos.y-circle.pos.y, 2)) < circle.size/2+ 70/2 +1)
      {
        pos = new PVector(random(70/2, 960 - 70/2), random(70/2, 720-70/2));
        j = -1;
      }
    }
    //while (-0.2<speed.x   && speed.x<0.2)
    //{
    //  speed.x = random(random(-1, 1)*3.9f);
    //}
    //while (-0.2<speed.y   && speed.y<0.2)
    //{
    //  speed.y = random(random(-1, 1)*3.9f);
    //}
    circles.add(new Circle(pos, speed, 70, color(channel, channel, channel)));
    Circle lastcircle = circles.get(i);
    lastcircle.posinlist = i+1;
  }
}
void setup()
{
  pixelDensity(2);
  size(960, 720);
  Reset();

  font = loadFont("Arial-BoldMT-32.vlw");
}
void draw()
{

  background(235, 235, 235);
  fill(0, 0, 0);
  noStroke();
  textFont(font);
  text(round(sum), 0, 32);
  sum = 0;

  for (Circle circle : circles)
  {


    sum += circle.number;
    circle.Draw();
    if (gameRun)
    {

      circle.Update();
      if (mousePressed)
      {
        if (sqrt(pow(mouseX - circle.pos.x, 2)+pow((mouseY -circle.pos.y), 2)) < circle.size/2)
        {

          if (circle.number < 100 && !circle.isstatic)
          {

            float newsize = circle.size+3;
            if (circle.pos.x - (newsize/2) > 0 && circle.pos.y - (newsize/2) > 0 && circle.pos.x + (newsize/2) <width && circle.pos.y + (newsize/2) < height)
            {
              circle._color = color(230, 0, 0);
              circle.size+=3;
            }
            if (circle.pos.x - (newsize/2) < 0)
            {
              circle.speed.x +=0.01f;
            }
            if (circle.pos.y - (newsize/2) < 0)
            {
              circle.speed.y +=0.01f;
            }
            if (circle.pos.x + (newsize/2) > 600)
            {
              circle.speed.x -=0.01f;
            }
            if (circle.pos.y + (newsize/2) > 600)
            {
              circle.speed.y -=0.01f;
            }
          }
        } else
        {
          circle._color = circle._originalcolor;
        }
      } else
      {
        circle._color = circle._originalcolor;
      }


      for (Circle othercircle : circles)
      {
        if (othercircle != circle && !circle.isstatic)
        {
          if (sqrt(pow(circle.pos.x + circle.speed.x- othercircle.pos.x-othercircle.speed.x, 2) + pow(circle.pos.y +circle.speed.y - othercircle.pos.y-othercircle.speed.y, 2)) <= circle.size/2+othercircle.size/2)
          {
            if (circle._color == color(230, 0, 0))
            {

              gameRun = false;
            } else
            {
              PVector oldspeed1 = circle.speed;
              PVector oldspeed2 = othercircle.speed;

              //swap speeds method
              // circle.speed = oldspeed2;
              //othercircle.speed = oldspeed1;
              //float mindist =
              // Get distances between the balls components
              PVector distanceVect = PVector.sub(othercircle.pos, circle.pos);

              // Calculate magnitude of the vector separating the balls
              float distanceVectMag = distanceVect.mag();

              // Minimum distance before they are touching
              float minDistance = circle.size/2 + othercircle.size/2;
              float distanceCorrection = (minDistance-distanceVectMag)/2.0;
              PVector d = distanceVect.copy();
              PVector correctionVector = d.normalize().mult(distanceCorrection);
              othercircle.pos.add(correctionVector);
              circle.pos.sub(correctionVector);

              // get angle of distanceVect
              float theta  = distanceVect.heading();
              // precalculate trig values
              float sine = sin(theta);
              float cosine = cos(theta);

              /* bTemp will hold rotated ball positions. You 
               just need to worry about bTemp[1] position*/
              PVector[] bTemp = {
                new PVector(), new PVector()
              };

              /* this ball's position is relative to the other
               so you can use the vector between them (bVect) as the 
               reference point in the rotation expressions.
               bTemp[0].position.x and bTemp[0].position.y will initialize
               automatically to 0.0, which is what you want
               since b[1] will rotate around b[0] */
              bTemp[1].x  = cosine * distanceVect.x + sine * distanceVect.y;
              bTemp[1].y  = cosine * distanceVect.y - sine * distanceVect.x;

              // rotate Temporary velocities
              PVector[] vTemp = {
                new PVector(), new PVector()
              };

              vTemp[0].x  = cosine * circle.speed.x + sine * circle.speed.y;
              vTemp[0].y  = cosine * circle.speed.y - sine * circle.speed.x;
              vTemp[1].x  = cosine * othercircle.speed.x + sine * othercircle.speed.y;
              vTemp[1].y  = cosine * othercircle.speed.y - sine * othercircle.speed.x;

              /* Now that velocities are rotated, you can use 1D
               conservation of momentum equations to calculate 
               the final velocity along the x-axis. */
              PVector[] vFinal = {  
                new PVector(), new PVector()
              };

              // final rotated velocity for b[0]
              vFinal[0].x = ((circle.mass - othercircle.mass) * vTemp[0].x + 2 * othercircle.mass * vTemp[1].x) / (circle.mass + othercircle.mass);
              vFinal[0].y = vTemp[0].y;

              // final rotated velocity for b[0]
              vFinal[1].x = ((othercircle.mass - circle.mass) * vTemp[1].x + 2 * circle.mass * vTemp[0].x) / (circle.mass + othercircle.mass);
              vFinal[1].y = vTemp[1].y;

              // hack to avoid clumping
              bTemp[0].x += vFinal[0].x;
              bTemp[1].x += vFinal[1].x;

              /* Rotate ball positions and velocities back
               Reverse signs in trig expressions to rotate 
               in the opposite direction */
              // rotate balls
              PVector[] bFinal = { 
                new PVector(), new PVector()
              };

              bFinal[0].x = cosine * bTemp[0].x - sine * bTemp[0].y;
              bFinal[0].y = cosine * bTemp[0].y + sine * bTemp[0].x;
              bFinal[1].x = cosine * bTemp[1].x - sine * bTemp[1].y;
              bFinal[1].y = cosine * bTemp[1].y + sine * bTemp[1].x;

              // update balls to screen position
              othercircle.pos.x = circle.pos.x + bFinal[1].x;
              othercircle.pos.y = circle.pos.y + bFinal[1].y;

              circle.pos.add(bFinal[0]);

              // update velocities
              if (!circle.isstatic)
              {
                circle.speed.x = cosine * vFinal[0].x - sine * vFinal[0].y;
                circle.speed.y = cosine * vFinal[0].y + sine * vFinal[0].x;
              }
              if (!othercircle.isstatic)
              {

                othercircle.speed.x = cosine * vFinal[1].x - sine * vFinal[1].y;
                othercircle.speed.y = cosine * vFinal[1].y + sine * vFinal[1].x;
              }
              //circle.pos.x += circle.speed.x;
              //circle.pos.y += circle.speed.y;
              // othercircle.pos.x +=othercircle.speed.x;
              //othercircle.pos.y += othercircle.speed.y;
              //if(circle.pos.x - circle.size/2 < othercircle.pos.x)
              //{
              //  circle.speed.x = -abs(circle.speed.x);
              //  //othercircle.speed.x = abs(othercircle.speed.x);
              //}
              //if(circle.pos.x + circle.size/2 > othercircle.pos.x)
              //{
              //  circle.speed.x = abs(circle.speed.x);
              //}
              //if(circle.pos.y - circle.size/2 < othercircle.pos.y)
              //{
              //  circle.speed.y = -abs(circle.speed.y);
              //}
              //if(circle.pos.y + circle.size/2 > othercircle.pos.y)
              //{
              //  circle.speed.y = abs(circle.speed.y);
              //}
            }
          }
        }
      }
    }
  }
  if (!gameRun)
  {

    if (keyCode == ' ' && lastKeyCode != ' ')
    {
      Reset();
    }
  }
  lastKeyCode = keyCode;
}
//void mouseDragged()
//{
//  for(Circle circle: circles)
//  {
//    if(sqrt(pow(mouseX - circle.pos.x,2)+pow((mouseY -circle.pos.x),2)) < circle.size)
//    {

//      if(circle.number < 100)
//      {

//        if(circle.pos.x - ((circle.size+5)/2) > 0)
//        {
//          circle._color = color(255,0,0);
//      circle.size+=5;
//        }
//      }
//    }

//  }
//}
