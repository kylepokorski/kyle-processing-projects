ArrayList<Circle> circles = new ArrayList<Circle>();
boolean gameRun = true;
float sum;
PFont font;
int lastKeyCode;
void Reset()
{
  //int seed = round(random(0,round(pow(2,32))));
  // randomSeed(seed);
  gameRun = true;
  circles.clear();
  //circles.add(new Circle(new PVector(150, 150), new PVector(1.5f, -1.8f), 70, color(100, 100, 100)));

  //circles.add(new Circle(new PVector(100, 100), new PVector(-1.6f, -1.5f), 70, color(50, 50, 50)));
  for (Integer i = 0; i< 22; i++)
  {

    float channel = random(0, 195);
    PVector pos = new PVector(random(70/2, 960 - 70/2), random(70/2, 720-70/2));
    PVector speed = new PVector(random(-1, 1)*2.9f, random(-1, 1)*2.9f);

    for (int j  = 0; j<circles.size(); j++)
    {
      Circle circle = circles.get(j);

      if (sqrt(pow(pos.x -circle.pos.x, 2) + pow(pos.y-circle.pos.y, 2)) < circle.size+ 70)
      {
        pos = new PVector(random(70/2, 960 - 70/2), random(70/2, 720-70/2));
        j = -1;
      }
    }
    while (-0.2<speed.x   && speed.x<0.2)
    {
      speed.x = random(random(-1, 1)*2.9f);
    }
    while (-0.2<speed.y   && speed.y<0.2)
    {
      speed.y = random(random(-1, 1)*2.9f);
    }
    circles.add(new Circle(pos, speed, 70, color(channel, channel, channel)));
    Circle lastcircle = circles.get(i);
    lastcircle.posinlist = i;
  }
}
void setup()
{
  pixelDensity(2);
  size(960, 720);
  Reset();

  font = loadFont("Arial-BoldMT-32.vlw");
}
void draw()
{

  background(235, 235, 235);
  fill(0, 0, 0);
  noStroke();
  textFont(font);
  text(round(sum), 0, 32);
  sum = 0;
 
  for (Circle circle : circles)
  {


   sum += circle.number;
    circle.Draw();
    if (gameRun)
    {

      circle.Update();
      if (mousePressed)
      {
        if (sqrt(pow(mouseX - circle.pos.x, 2)+pow((mouseY -circle.pos.y), 2)) < circle.size/2)
        {

          if (circle.number < 100)
          {

            float newsize = circle.size+3;
            if (circle.pos.x - (newsize/2) > 0 && circle.pos.y - (newsize/2) > 0 && circle.pos.x + (newsize/2) <width && circle.pos.y + (newsize/2) < height)
            {
              circle._color = color(230, 0, 0);
              circle.size+=3;
            }
            if (circle.pos.x - (newsize/2) < 0)
            {
              circle.speed.x +=0.01f;
            }
            if (circle.pos.y - (newsize/2) < 0)
            {
              circle.speed.y +=0.01f;
            }
            if (circle.pos.x + (newsize/2) > 600)
            {
              circle.speed.x -=0.01f;
            }
            if (circle.pos.y + (newsize/2) > 600)
            {
              circle.speed.y -=0.01f;
            }
          }
        } else
        {
          circle._color = circle._originalcolor;
        }
      } else
      {
        circle._color = circle._originalcolor;
      }


      for (Circle othercircle : circles)
      {
        if (othercircle != circle)
        {
          if (sqrt(pow(circle.pos.x- othercircle.pos.x, 2) + pow(circle.pos.y - othercircle.pos.y, 2)) < circle.size/2+othercircle.size/2)
          {
            if (circle._color == color(230, 0, 0))
            {

              gameRun = false;
            } else
            {
              PVector oldspeed1 = circle.speed;
              PVector oldspeed2 = othercircle.speed;

//swap speeds method
             // circle.speed = oldspeed2;
              //othercircle.speed = oldspeed1;
              //float mindist =
              PVector d = PVector.sub(othercircle.pos,circle.pos);
              float correct = (circle.size/2 - othercircle.size/2-d.mag())/2;
              PVector correctv = d.normalize().mult(correct);
              othercircle.pos.add(correctv);
              circle.pos.sub(correctv);
              float angle = d.heading();
              
              PVector c1temp;
               PVector c0temp = new PVector(0,0);
               
               
              
              c1temp =new PVector(cos(angle)*d.x + sin(angle)*d.y, cos(angle)*d.y -sin(angle)*d.x);
              PVector v1temp;
              PVector v0temp;
              
              v0temp = new PVector(cos(angle)*circle.speed.x + sin(angle)*circle.speed.y,cos(angle)*circle.speed.y - sin(angle)*circle.speed.x);
               v1temp = new PVector(cos(angle)*othercircle.speed.x + sin(angle)*othercircle.speed.y,cos(angle)*othercircle.speed.y - sin(angle)*othercircle.speed.x);
               
               PVector v1final = new PVector();
               PVector v0final = new PVector();
               
               v0final.y = v0temp.y;
               v0final.x = ((circle.mass -othercircle.mass)*v0temp.x + 2*othercircle.mass*v1temp.x)/(othercircle.mass+circle.mass);
               
                v1final.y = v1temp.y;
               v1final.x = ((othercircle.mass -circle.mass)*v1temp.x + 2*circle.mass*v0temp.x)/(othercircle.mass+circle.mass);
               
               c1temp.x += v1final.x;
                c0temp.x += v0final.x;
                
                
                PVector c0final  = new PVector(cos(angle)*c0temp.x - sin(angle)*c0temp.y,cos(angle)*c0temp.x+sin(angle)*c0temp.y);
                PVector c1final = new PVector(cos(angle)*c1temp.x - sin(angle)*c1temp.y,cos(angle)*c1temp.x+sin(angle)*c1temp.y);
               othercircle.pos.x = circle.pos.x + c1final.x;
               othercircle.pos.y = circle.pos.y + c1final.y;
               circle.pos.add(c0final);
               circle.speed.x = cos(angle)*v0final.x- sin(angle)*v0final.y;
              circle.speed.y = cos(angle)*v0final.y+ sin(angle)*v0final.x;
              
                 othercircle.speed.x = cos(angle)*v1final.x- sin(angle)*v1final.y;
              othercircle.speed.y = cos(angle)*v1final.y+ sin(angle)*v1final.x;
              //circle.pos.x += circle.speed.x;
              //circle.pos.y += circle.speed.y;
            // othercircle.pos.x +=othercircle.speed.x;
              //othercircle.pos.y += othercircle.speed.y;
              //if(circle.pos.x - circle.size/2 < othercircle.pos.x)
              //{
              //  circle.speed.x = -abs(circle.speed.x);
              //  //othercircle.speed.x = abs(othercircle.speed.x);
              //}
              //if(circle.pos.x + circle.size/2 > othercircle.pos.x)
              //{
              //  circle.speed.x = abs(circle.speed.x);
              //}
              //if(circle.pos.y - circle.size/2 < othercircle.pos.y)
              //{
              //  circle.speed.y = -abs(circle.speed.y);
              //}
              //if(circle.pos.y + circle.size/2 > othercircle.pos.y)
              //{
              //  circle.speed.y = abs(circle.speed.y);
              //}
            }
          }
        }
      }
    }
  }
  if (!gameRun)
  {

    if (keyCode == ' ' && lastKeyCode != ' ')
    {
      Reset();
    }
  }
  lastKeyCode = keyCode;
}
//void mouseDragged()
//{
//  for(Circle circle: circles)
//  {
//    if(sqrt(pow(mouseX - circle.pos.x,2)+pow((mouseY -circle.pos.x),2)) < circle.size)
//    {

//      if(circle.number < 100)
//      {

//        if(circle.pos.x - ((circle.size+5)/2) > 0)
//        {
//          circle._color = color(255,0,0);
//      circle.size+=5;
//        }
//      }
//    }

//  }
//}
