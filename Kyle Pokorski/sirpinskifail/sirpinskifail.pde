void setup()
{
  noFill();
  size(500, 500);
}
void draw()
{

  generate(new PVector(300, 200),50, 5, 0, 0);
}
void generate(PVector point1, float _length, float strokewidth, int times, float change)
{
  if (times < 7)
  {

    strokeWeight(strokewidth);
    line(point1.x,point1.y, point1.x -(int)(sqrt(2)*change),point1.y-(int)_length);
     //line(point1.x,point1.y, point1.x +(int)(sqrt(3)*change),point1.y-(int)_length);
     // line(point1.x,point1.y, point1.x +(int)(sqrt(3)*change),point1.y-(int)_length);
    generate(new PVector(point1.x-(int)(sqrt(2)*change),point1.y-(_length)),_length*0.7,(int)(0.7*strokewidth),times+1,change +10);
    generate(new PVector(point1.x-(int)(sqrt(2)*change),point1.y-(_length)),_length*0.7,(int)(0.7*strokewidth),times+1,change -10);
     //generate(new PVector(point1.x+(int)(sqrt(3)*change),point1.y-(_length)),_length*0.7,(int)(0.7*strokewidth),times+1,change);
  
    // generate(new PVector(point1.x-(int)(sqrt(3)*2*times),point1.y-(_length)),_length*0.7,(int)(0.7*strokewidth),times+1,change);
  }
}
