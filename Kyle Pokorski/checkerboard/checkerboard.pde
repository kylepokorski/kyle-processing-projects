size(512, 512);
fill(40, 50, 230);
stroke(50, 40, 70);
noStroke();
background(255, 50, 70);
for (int i = 0; i<6; i++)
{

  triangle(i*80, i*80, 50 +i*80, 50+i*80, 8+i*80, 80+i*80);
}
for (int i = 0; i<8; i++)
{
  for (int j = 0; j<8; j++)
  {
    if (i%2 == 0 && j%2== 0|| i%2 == 1 && j%2==1)
    {
      fill(0, 0, 0);
      rect(i*width/8, j*height/8, width/8, height/8);
    } else
    {
      fill(255, 255, 255);
      rect(i*width/8, j*height/8, width/8, height/8);
    }
  }
}

for (int i = 0; i<8; i++)
{
  for (int j = 0; j<8; j++)
  {
    if (i %2 == 0)
    {
      if (i%2 == 0 && j%2== 0|| i%2 == 1 && j%2==1)
      {
        fill(0, 0, 0);
        rect(i*width/8, j*height/8, width/8, height/8);
      } else
      {
        fill(255, 255, 255);
        rect(i*width/8, j*height/8, width/8, height/8);
      }
    } 
    else
    {
      if (i%2 == 0 && j%2== 0|| i%2 == 1 && j%2==1)
      {
        fill(50, 50, 50);
        rect(i*width/8, j*height/8, width/8, height/8);
      } else
      {
        fill(128,128, 128);
        rect(i*width/8, j*height/8, width/8, height/8);
      }
    }
  }
}

fill(128, 128, 128);
stroke(67, 50, 50);
noFill();
arc(80, 80, 80, 80, PI/4, PI);
//rect(50,5,90,40);
//quad(300,300,250,200,100,40,400,200);
//line(60,70,50,70);
//circle(70,70,50);
