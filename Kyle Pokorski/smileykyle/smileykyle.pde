float arcsize = 50;
void setup()
{
  size(500, 500);
  background(255, 255, 255);
}

void draw()
{
  background(255, 255, 255);

  fill(255, 255, 0);
  circle(225, 225, 100);
  noStroke();
  fill(0, 0, 0);
  circle(205, 210, 20);
  circle(245, 210, 20);
  noFill();
  stroke(0, 0, 0);
  strokeWeight(5);
  if(sqrt(pow(mouseX - 225,2) + pow(mouseY - 225,2)) <= 50)
  {
    if (arcsize > 1)
    {
      arcsize--;
    }
    
  }
  else
  {
    if(arcsize <= 50)
    {
      arcsize++;
    }
  }
  if (mouseX >= 175 && mouseX <= 275 && mouseY >= 175 && mouseY <= 275)
  {
    
   
  }
  else
  {
    
  }

  arc(225, 235, 50, arcsize, 0f, (float)Math.PI);
}
