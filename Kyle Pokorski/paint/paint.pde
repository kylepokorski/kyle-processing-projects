int size = 3;
boolean sizeincrease = false;
boolean sizedecrease = false;
void setup()
{
  size(700, 700);
  background(0, 40, 125);
  noStroke();
}
void mouseDragged()
{
  rect(mouseX, mouseY, size, size);
}

void draw()
{
  if (keyPressed)
  {
    if (key == '+' && !sizeincrease)
    {
      
      size++;
      sizeincrease = true;
    }
    if(key == '-' && !sizedecrease)
    {
      if(size > 1)
      {
        size--;
        sizedecrease = true;
      }
    }
    if( key == 'C')
    {
      background(0,40,125);
    }
    if(key == 'r')
    {
      fill(255,0,0);
    }
    if(key == 'b')
    {
      fill(0,0,255);
    }
    if(key == 'c')
    {
      fill(0,255,255);
    }
  }
  else
 {
   
   sizeincrease = false;
   sizedecrease = false;
 }
 
  

  //circle(300,300,300);

  //mouseDragged();
}
