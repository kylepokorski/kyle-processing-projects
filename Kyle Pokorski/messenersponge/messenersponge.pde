void setup()
{
  size(721, 721);
}
void draw()
{
  background(190, 190, 190);
  generate(240, 0, new PVector(0,0));
}
void generate(float size, int times, PVector pos)
{
  if (times < 5)
  {
    rect(pos.x, pos.y, size, size);
    rect(pos.x + size, pos.y, size, size);
    rect(pos.x + size*2, pos.y, size, size);
    rect(pos.x, pos.y+ size, size, size);
    rect(pos.x, pos.y + size*2, size, size);
    rect(pos.x + size, pos.y + size*2, size, size);
    rect(pos.x + size*2, pos.y + size*2, size, size);
    rect(pos.x + size*2, pos.y + size, size, size);
    generate(size/3, times+1,new PVector(pos.x,pos.y));
    generate(size/3, times+1,new PVector(pos.x + size,pos.y));
    generate(size/3, times+1,new PVector(pos.x,pos.y + size));
    
    generate(size/3, times+1,new PVector(pos.x+size*2,pos.y));
    generate(size/3, times+1,new PVector(pos.x + size*2,pos.y+size));
    generate(size/3, times+1,new PVector(pos.x, pos.y + size*2));
     generate(size/3, times+1,new PVector(pos.x+size, pos.y + size*2));
     generate(size/3, times+1,new PVector(pos.x+size*2, pos.y + size*2));
  }
}
