class ThemeScreen extends Screen
{
  MainScreen main;
  Slider sliderR;
  Slider sliderG;
  Slider sliderB;


  Slider rightsliderR;
  Slider rightsliderG;
  Slider rightsliderB;


  Slider sliderendR;
  Slider sliderendG;
  Slider sliderendB;
  Slider sliderendA;

  Slider rightsliderendR;
  Slider rightsliderendG;
  Slider rightsliderendB;
  Slider rightsliderendA;


  Slider backgroundR;
  Slider backgroundG;
  Slider backgroundB;
  PFont font;
  public ThemeScreen(MainScreen main)
  {

    super(color(0, 128, 128), "Theme");
    this.main =main;
    font = loadFont("Arial-BoldMT-20.vlw");
    sliderR = new Slider(new PVector(200, 150), 150, 250, 250, 20, 2, true, "Theme");
    sliderG = new Slider(new PVector(250, 150), 150, 250, 250, 20, 2, true, "Theme");
    sliderB = new Slider(new PVector(300, 150), 150, 250, 250, 20, 2, true, "Theme");


    rightsliderR = new Slider(new PVector(550, 150), 150, 250, 250, 20, 2, true, "Theme");
    rightsliderG = new Slider(new PVector(600, 150), 150, 250, 250, 20, 2, true, "Theme");
    rightsliderB = new Slider(new PVector(650, 150), 150, 250, 250, 20, 2, true, "Theme");


    sliderendR = new Slider(new PVector(200, 300), 300, 400, 300, 20, 2, true, "Theme");
    sliderendG = new Slider(new PVector(250, 300), 300, 400, 300, 20, 2, true, "Theme");
    sliderendB = new Slider(new PVector(300, 300), 300, 400, 300, 20, 2, true, "Theme");
    sliderendA = new Slider(new PVector(350, 300), 300, 400, 300, 20, 2, true, "Theme");

    rightsliderendR = new Slider(new PVector(550, 300), 400, 300, 300, 20, 2, true, "Theme");
    rightsliderendG = new Slider(new PVector(600, 300), 400, 300, 300, 20, 2, true, "Theme");
    rightsliderendB = new Slider(new PVector(650, 300), 400, 300, 300, 20, 2, true, "Theme");
    rightsliderendA = new Slider(new PVector(700, 300), 400, 300, 300, 20, 2, true, "Theme");

    backgroundR = new Slider(new PVector(200, 450), 450, 550, 550, 20, 2, true, "Theme");
    backgroundG = new Slider(new PVector(250, 450), 450, 550, 550, 20, 2, true, "Theme");
    backgroundB = new Slider(new PVector(300, 450), 450, 550, 550, 20, 2, true, "Theme");
    backgroundR.MoveSlider(1);
    backgroundG.MoveSlider(0.3);
    backgroundB.MoveSlider(0);
  }
  public void Draw()
  {

    super.Draw();

    fill(255, 255, 255);
    textFont(font);
    text("1. Theme Type:", 10, 50);
    text("Left Start", 10, 200);
    text("Right Start", 375, 200);
    text("Left End", 10, 350);
    text("Right End", 375, 350);
    text("Background", 0, 495);
    text("Back to Settings", 10, 650);
    if (main.theme == 0)
    {
      fill(255, 0, 0);
      text("Classic", 160, 50);
      fill(255, 255, 255);
      text("Natural", 260, 50);
      text("Custom", 360, 50);
      text("Retro", 460, 50);
      text("Inverted", 540, 50);
    }
    if (main.theme == 1)
    {
      fill(255, 255, 255);
      text("Classic", 160, 50);

      fill(255, 0, 0);
      text("Natural", 260, 50);
      fill(255, 255, 255);
      text("Custom", 360, 50);
      text("Retro", 460, 50);
      text("Inverted", 540, 50);
    }
    if (main.theme == 2)
    {
      fill(255, 255, 255);
      text("Classic", 160, 50);


      text("Natural", 260, 50);
      fill(255, 0, 0);
      text("Custom", 360, 50);
      fill(255, 255, 255);
      text("Retro", 460, 50);
      text("Inverted", 540, 50);
    }
    if (main.theme == 3)
    {
      fill(255, 255, 255);
      text("Classic", 160, 50);


      text("Natural", 260, 50);

      text("Custom", 360, 50);
      fill(255, 0, 0);
      text("Retro", 460, 50);
      fill(255, 255, 255);
      text("Inverted", 540, 50);
    }
    if (main.theme == 4)
    {
      fill(255, 255, 255);
      text("Classic", 160, 50);


      text("Natural", 260, 50);

      text("Custom", 360, 50);

      text("Retro", 460, 50);
      fill(255, 0, 0);
      text("Inverted", 540, 50);
    }
    sliderR.Draw();
    sliderB.Draw();
    sliderG.Draw();
    fill(color(255 -sliderR.slidedistancepercentage()*255, 255-sliderG.slidedistancepercentage()*255, 255- sliderB.slidedistancepercentage()*255));

    rect(120, 175, 50, 50);
    if (main.theme == 2)
    {
      main.start = color(255 -sliderR.slidedistancepercentage()*255, 255-sliderG.slidedistancepercentage()*255, 255- sliderB.slidedistancepercentage()*255);
      main.rightstart =color(255 -rightsliderR.slidedistancepercentage()*255, 255-rightsliderG.slidedistancepercentage()*255, 255- rightsliderB.slidedistancepercentage()*255);
      main.end = color(255 -sliderendR.slidedistancepercentage()*255, 255-sliderendG.slidedistancepercentage()*255, 255- sliderendB.slidedistancepercentage()*255, 255-sliderendA.slidedistancepercentage()*255);
      main.rightend = color(rightsliderendR.slidedistancepercentage()*255, rightsliderendG.slidedistancepercentage()*255, rightsliderendB.slidedistancepercentage()*255, rightsliderendA.slidedistancepercentage()*255);
      main.backgroundcolor = color(255 -backgroundR.slidedistancepercentage()*255, 255-backgroundG.slidedistancepercentage()*255, 255-backgroundB.slidedistancepercentage()*255);
    }
    rightsliderR.Draw();
    rightsliderB.Draw();
    rightsliderG.Draw();
    fill(color(255 -rightsliderR.slidedistancepercentage()*255, 255-rightsliderG.slidedistancepercentage()*255, 255- rightsliderB.slidedistancepercentage()*255));
    rect(480, 175, 50, 50);

    sliderendR.Draw();
    sliderendG.Draw();
    sliderendB.Draw();
    sliderendA.Draw();
    fill(color(255 -sliderendR.slidedistancepercentage()*255, 255-sliderendG.slidedistancepercentage()*255, 255- sliderendB.slidedistancepercentage()*255, 255-sliderendA.slidedistancepercentage()*255));

    rect(120, 325, 50, 50);

    rightsliderendR.Draw();
    rightsliderendG.Draw();
    rightsliderendB.Draw();
    rightsliderendA.Draw();

    fill(color(rightsliderendR.slidedistancepercentage()*255, rightsliderendG.slidedistancepercentage()*255, rightsliderendB.slidedistancepercentage()*255, rightsliderendA.slidedistancepercentage()*255));
    rect(480, 325, 50, 50);

    backgroundR.Draw();
    backgroundG.Draw();
    backgroundB.Draw();
    fill(color(255 -backgroundR.slidedistancepercentage()*255, 255-backgroundG.slidedistancepercentage()*255, 255-backgroundB.slidedistancepercentage()*255));

    rect(120, 475, 50, 50);
  }
}
