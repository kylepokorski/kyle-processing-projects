import processing.svg.*; //<>//

//public static ScreenManager manager;
MainScreen main;
SettingsScreen settings;
ThemeScreen theme;
PFont font;

void setup()
{

  Global.manager = new ScreenManager();
  main = new MainScreen(color(190, 190, 190), "Main");
  settings = new SettingsScreen(main);
  theme = new ThemeScreen(main);
  Global.manager.Add(main);
  Global.manager.Add(settings);
  Global.manager.Add(theme);
  Global.manager.screentype = "Theme";
  font = loadFont("Arial-BoldMT-20.vlw");
  Global.json = loadJSONObject("settings.json");
  noSmooth();
}
void draw()
{


  Global.manager.Draw();
}
void settings()
{
  pixelDensity(displayDensity());
  //pixelDensity(1);
  size(750, 750);
  if (!Global.smooth)
  {
    noSmooth();
  } else
  {
    smooth();
  }
}
void mouseDragged()
{
  GlobalUpdate();
  //if (Global.manager.screentype == "Main")
  //{
  //  main.angleleft+=0.01;
  //}
}
void mouseClicked()
{

  if (Global.manager.screentype == "Main")
  {
    main.move = !main.move;
  }
  if (sqrt(pow(mouseX - 400, 2) + pow(mouseY, 2)) < 60 && Global.manager.screentype == "Main")
  {
    Global.manager.screentype = "Settings";
  }
  if (sqrt(sq(mouseX-600) + sq(mouseY)) < 60 && Global.manager.screentype == "Main")
  {
    save("tree.png");
  }
  if (sqrt(sq(mouseX -25) + sq(mouseY - 725)) < 25 && Global.manager.screentype == "Main")
  {
    if (main.scale > 0.1f)
    {
      main.scale -= 0.05f;
    }
  }
  if (sqrt(sq(mouseX-110) + sq(mouseY - 725)) <25 && Global.manager.screentype == "Main")
  {

    if (main.scale < 3f )
    {
      main.scale+= 0.05f;
    }
  }
  if ( mouseX >= 10 && mouseY >= 670 && mouseX <= 90 && mouseY <=700 && Global.manager.screentype == "Settings")
  {
    Global.manager.screentype = "Main";
  }
  if (Global.manager.screentype == "Theme")
  {
    if (mouseX >= 160 && mouseX <=230 && mouseY >= 30 && mouseY <= 50)
    {
      main.theme = 0;
    }
    if (mouseX >= 260 && mouseX <= 330 && mouseY >= 30 && mouseY <=50)
    {
      main.theme =1;
    }
    if (mouseX >= 360 && mouseX <=430 && mouseY >= 30 && mouseY <= 50)
    {
      main.theme = 2;
    }
    if (mouseX >= 460 && mouseX <=512 && mouseY >=30 && mouseY <= 50)
    {
      main.theme = 3;
    }
    if (mouseX >= 540 && mouseX <=620 && mouseY >= 30 && mouseY <= 50)
    {
      main.theme = 4;
    }
    if (mouseX >= 10 && mouseX <=170 && mouseY >=620 && mouseY <= 650)
    {
      Global.manager.screentype = "Settings";
    }
  }
  if (Global.manager.screentype == "Settings")
  {
    if (mouseX >=250 && mouseX <=380 && mouseY >= 710 && mouseY <=740)
    {
      Global.manager.screentype = "Theme";
    }
    if (mouseX >=475 && mouseX <= 525 && mouseY >= 10 && mouseY <= 60)
    {
      if (main._lengthdecreaseright > 0.2f)
      {
        main._lengthdecreaseright -=0.01f;
      }
    }
    if (mouseX >=565 && mouseX <= 590 && mouseY >= 10 && mouseY <= 60)
    {
      if (main._lengthdecreaseright <1.5f)
      {
        main._lengthdecreaseright +=0.01f;
      }
    }
    if (mouseX >=330 && mouseX <= 355 && mouseY >= 10 && mouseY <= 60 )
    {
      if (main._lengthdecrease < 1.5f)
      {
        main._lengthdecrease += 0.01;
      }
    }
    if (mouseX>= 245 && mouseX<= 270 && mouseY>=10 && mouseY <= 60)
    {
      if (main._lengthdecrease > 0.2f)
      {
        main._lengthdecrease -=0.01f;
      }
    }
    if (mouseX >= 330 && mouseX <= 355 && mouseY >= 110 && mouseY <= 160)
    {
      if (main.originaliterations < 19)
      {
        main.originaliterations ++;
        if (!main.growmode)
        {

          main.iterations = main.originaliterations;
        }
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 110 && mouseY <=160)
    {
      if (main.originaliterations > 2)
      {
        main.originaliterations--;
        if (!main.growmode)
        {

          main.iterations = main.originaliterations;
        }
      }
    }
    if (mouseY >= 80 && mouseY <= 100 && mouseX >=250 && mouseX <=290)
    {
      main.circlemode = true;
      main.addnodemode = false;
    }
    if (mouseY >= 80 && mouseY <= 100 && mouseX>= 350 && mouseX <=395)
    {
      main.circlemode = false;
    }
    if (mouseY >= 180 && mouseY <= 200 && mouseX >=250 && mouseX <=290)
    {
      main.changewidth = true;
    }
    if (mouseY >= 180 && mouseY <= 210 && mouseX>= 350 && mouseX <=395)
    {
      main.changewidth = false;
    }
    if (mouseY >= 480 && mouseY <= 500 && mouseX >=250 && mouseX <=290)
    {
      main.compensate = true;
    }
    if (mouseY >= 480 && mouseY <= 510 && mouseX>= 350 && mouseX <=395)
    {
      main.compensate = false;
    }
    if (mouseY >= 530 && mouseY <= 550 && mouseX >=250 && mouseX <=290)
    {
      main.seperateangleright = true;
    }
    if (mouseY >= 530 && mouseY <= 560 && mouseX>= 350 && mouseX <=395)
    {
      main.seperateangleright = false;
    }
    if (mouseY >= 580 && mouseY <= 600 && mouseX >=250 && mouseX <=290)
    {
      main.addnodemode = true;
      main.circlemode = false;
    }
    if (mouseY >= 580 && mouseY <= 610 && mouseX>= 350 && mouseX <=395)
    {
      main.addnodemode = false;
    }
    if (mouseY >= 630 && mouseY <= 650 && mouseX >=250 && mouseX <=290)
    {
      main.dualtree = true;
    }
    if (mouseY >= 630 && mouseY <= 660 && mouseX>= 350 && mouseX <=395)
    {
      main.dualtree = false;
    }


    if (mouseX >= 330 && mouseX <= 355 && mouseY >= 210 && mouseY <= 260)
    {
      if (main._width < 20)
      {
        main._width ++;
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 210 && mouseY <=260)
    {
      if (main._width > 0)
      {
        main._width--;
      }
    }
    if (mouseX >= 330 && mouseX <= 355 && mouseY >= 260 && mouseY <= 310)
    {
      if (main._Length < 130)
      {
        main._Length ++;
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 260 && mouseY <=310)
    {
      if (main._Length > 1)
      {
        main._Length--;
      }
    }
    if (mouseX >=475 && mouseX <= 525 && mouseY >= 260 && mouseY <= 310)
    {
      if (main._LengthRight > 1)
      {
        main._LengthRight --;
      }
    }
    if (mouseX >=565 && mouseX <= 590 && mouseY >= 260 && mouseY <= 310)
    {
      if (main._LengthRight <140)
      {
        main._LengthRight ++;
      }
    }
    if (mouseX >= 330 && mouseX <= 355 && mouseY >= 310 && mouseY <= 360)
    {
      if (main._anglechangeleft < 0.05f)
      {
        main._anglechangeleft +=0.005f;
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 310 && mouseY <=360)
    {
      if (main._anglechangeleft >-0.005)
      {
        main._anglechangeleft-=0.005f;
      }
    }
    if (mouseX >=475 && mouseX <= 525 && mouseY >= 310 && mouseY <= 360)
    {
      if (main._anglechangeright > -0.005)
      {
        main._anglechangeright -=0.005;
      }
    }
    if (mouseX >=565 && mouseX <= 590 && mouseY >= 310 && mouseY <= 360)
    {
      if (main._anglechangeright <0.05f)
      {
        main._anglechangeright +=0.005;
      }
    }
    if (mouseX >= 330 && mouseX <= 355 && mouseY >= 360 && mouseY <= 410)
    {
      if (main.branches < 9 && !main.seperateangleright || main.branches < 3 && main.seperateangleright)
      {
        main.branches ++;
      }
    }
    if (main.branches > 3 && main.seperateangleright)
    {
      main.branches = 3;
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 360 && mouseY <=410)
    {
      if (main.branches > 1)
      {
        main.branches--;
      }
    }
    if (mouseX >= 330 && mouseX <= 355 && mouseY >= 410 && mouseY <= 450)
    {
      if (main.symmetry < 20)
      {
        main.symmetry++;
      }
    }
    if (mouseX >= 245 && mouseX <= 270 && mouseY >= 410 && mouseY <=450)
    {
      if (main.symmetry > 1)
      {
        main.symmetry--;
      }
    }
    if (mouseX>= 500 && mouseX <=715 && mouseY >= 520 && mouseY <= 550)
    {
      main.branches = 2;
      main._anglechangeright = 0.01f;
      main._anglechangeleft = 0.01f;
      main._Length  = 100;
      main._LengthRight  = 100;
      main._width = 10;
      main.angleright= 0;
      main.angleleft = 0;
      main.startangleright= 0;
      main.startangleleft = 0;
      main.compensate = true;
      main.circlemode = false;
      main.originaliterations = 10;
       if (!main.growmode)
      {
        main.iterations = main.originaliterations;
      }
      main._lengthdecrease = 0.8f;
      main._lengthdecreaseright = 0.8f;
      main.seperateangleright = false;
      main.addnodemode = false;
      main.dualtree = false;
    }
    if (mouseX >= 500 && mouseX <=665 && mouseY >=580 && mouseY <= 600)
    {
      main.branches = (int)random(1, 10);
      main.originaliterations = (int)(random(2, 14));
      if (!main.growmode)
      {
        main.iterations = main.originaliterations;
      }
      main._anglechangeright = random(-0.005, 0.05);
      main._anglechangeleft = random(-0.005, 0.05);
      main._lengthdecrease = random(0.3, 1.1);
      main._lengthdecreaseright = random(0.3, 1.1);
      main._Length = (int)random(20, 140);
      main._LengthRight = (int)random(20, 140);
      if ((int)random(0, 2) == 0)
      {
        main.circlemode = true;
      } else
      {
        main.circlemode = false;
      }
      if ((int)random(0, 2) == 0)
      {
        main.changewidth = true;
      } else
      {
        main.changewidth = false;
      }
    }
    if (mouseX >= 500 && mouseX <=630 && mouseY >= 630 && mouseY <= 650)
    {
      Global.json.setFloat("Length Decrease Left", main._lengthdecrease);
      Global.json.setFloat("Length Decrease Right", main._lengthdecreaseright);
      Global.json.setBoolean("Circle Mode", main.circlemode);
      Global.json.setInt("Iterations", main.originaliterations);
      Global.json.setBoolean("Reduce Width", main.changewidth);
      Global.json.setFloat("Width", main._width);
      Global.json.setInt("Length Left", main._Length);
      Global.json.setInt("Length Right", main._LengthRight);
      Global.json.setFloat("Angle Change Left", main._anglechangeleft);
      Global.json.setFloat("Angle Change Right", main._anglechangeright);
      Global.json.setInt("Branches", main.branches);
      Global.json.setInt("Symmetry", main.symmetry);
      Global.json.setBoolean("Compensate", main.compensate);
      Global.json.setBoolean("Add Right Angle", main.seperateangleright);
      Global.json.setBoolean("Add Nodes", main.addnodemode);
      Global.json.setBoolean("Dual Tree", main.dualtree);
      Global.json.setFloat("Scale", main.scale);
      saveJSONObject(Global.json, "settings.json");
    }
    if (mouseX >= 500 && mouseX <=630 && mouseY >= 680 && mouseY<= 700)
    {

      main._lengthdecrease = Global.json.getFloat("Length Decrease Left");
      main._lengthdecreaseright = Global.json.getFloat("Length Decrease Right");
      main.circlemode = Global.json.getBoolean("Circle Mode");
      main.originaliterations = Global.json.getInt("Iterations");
      main.changewidth = Global.json.getBoolean("Reduce Width");
      main._width = Global.json.getFloat("Width");
      main._Length = Global.json.getInt("Length Left");

      main._LengthRight = Global.json.getInt("Length Right");
      main._anglechangeleft = Global.json.getFloat("Angle Change Left");
      main._anglechangeright = Global.json.getFloat("Angle Change Right");
      main.branches = Global.json.getInt("Branches");
      main.symmetry = Global.json.getInt("Symmetry");
      main.compensate = Global.json.getBoolean("Compensate");
      main.seperateangleright = Global.json.getBoolean("Add Right Angle");
      main.addnodemode = Global.json.getBoolean("Add Nodes");
      main.dualtree = Global.json.getBoolean("Dual Tree");
      main.scale = Global.json.getFloat("Scale");
      if (!main.growmode)
      {
        main.iterations = main.originaliterations;
      }
    }
  }
}
