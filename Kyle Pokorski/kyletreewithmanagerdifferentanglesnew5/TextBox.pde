class TextBox
{
  PFont font;
  String text;
  PVector position;
  color tint;
  public TextBox(PFont font,String text, PVector position, color tint)
  {
    this.font = font;
    this.text = text;
    this.position = position;
    this.tint = tint;
  }
  public void Draw()
  {
    fill(tint);
    textFont(font);
    text(text,position.x,position.y);
  }
 
}
