float angle1 = 0;
float angle2 = 0;
PVector pos = new PVector(500, 70, 0);

void setup()
{
  size(600, 600, P3D);
}
void draw()
{
  angle1 = -mouseY/(float)(height);
  angle2= -mouseX/(float)(width);
    background(190, 190, 190);
  pushMatrix();
  translate(pos.x, pos.y, pos.z);
  rotateX(angle1);
  rotateY(angle2);
  strokeWeight(1);
  // line(0, 0, 0, 40, 50, 60);
  //noStroke();
  box(10, 20, 10); 
  popMatrix();
}
