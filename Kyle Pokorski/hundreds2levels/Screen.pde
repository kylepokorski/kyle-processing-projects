public class Screen
{

 public color backgroundcolor;
  public String screenName;
  public boolean debugmode = false;
  PFont font;
  boolean pressed = false;
  public  Screen (color backgroundcolor, String screenName)
  {
    this.backgroundcolor = backgroundcolor;
    this.screenName = screenName;
    
  }
  public void Draw()
  {
    if (keyPressed)
    {
      if (key == 'D' && !pressed)
      {
        pressed =true;
        debugmode = !debugmode;
      }
    } else
    {
      pressed = false;
    }
    background(backgroundcolor);
    if (debugmode)
    {
      noStroke();
      fill(0, 0, 0);
      text(mouseX, 500, 20);
      text(",", 530, 20);
      text(mouseY, 540, 20);
    }
  }
  public void onMouseClick()
  {
    
  }
}
