public class ScreenManager
{
  
  ArrayList<Screen> screens = new ArrayList<Screen>();
  public  ScreenManager()
  {
  
  }
  public String screentype;
  public void Add(Screen screen)
  {
    screens.add(screen);
  }
  public void Draw()
  {
  
    for(Screen screen: screens)
    {
      if(screen.screenName == screentype)
      {
        screen.Draw();
      }
    }
  }
  public void onMouseClick()
  {
    for(Screen screen: screens)
    {
      if(screen.screenName == screentype)
      {
        screen.onMouseClick();
      }
    }
  }
}
