ArrayList<Circle> circles = new ArrayList<Circle>();
boolean gameRun = true;

float alpha = 0;

GameScreen game;
MainMenuScreen mainmenu;
LevelSelectorScreen levelselect;

void setup()
{
  pixelDensity(2);
  size(960, 720);
  Global.manager = new ScreenManager();
  game = new GameScreen();
    game.setTheme();
  mainmenu = new MainMenuScreen();
  levelselect = new LevelSelectorScreen();

  Global.manager.Add(game);
  Global.manager.Add(mainmenu);
  Global.manager.Add(levelselect);
  
  Global.manager.screentype = "MainMenu";


}
void draw()
{
Global.manager.Draw();

 
  

}
void mouseClicked()
{
 Global.manager.onMouseClick(); 
}
