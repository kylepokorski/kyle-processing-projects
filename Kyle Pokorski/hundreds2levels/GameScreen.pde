public class GameScreen extends Screen
{
  ArrayList<Circle> circles = new ArrayList<Circle>();
  boolean gameRun = true;
  float sum;
  PFont font;
  int lastKeyCode;
  RectangleObstacle rect;
  Circle collidedcircle1;
  Circle collidedcircle2;
  float alpha = 0;
  boolean holdtogrow = true;
  int theme = 0;
  // color backgroundColor = color(235, 235, 235);
  color textcolor = color(0);
   color mincirclecolor = color(0, 0, 0);
  color maxcirclecolor = color(195, 195, 195);

  float millis;
  float prevmillis;
  // public color selectedcolor = color(230,0,0);
  //0 - classic
  //1
  //2 - 
 // Levels levels;
  void Reset()
  {
    setTheme();
    alpha = 0;
    //int seed = round(random(0,round(pow(2,32))));
    // randomSeed(seed);
    gameRun = true;
    circles.clear();
    //circles.add(new Circle(new PVector(150, 150), new PVector(1.5f, -1.8f), 70, color(100, 100, 100)));

    //circles.add(new Circle(new PVector(100, 100), new PVector(0,0), 70, color(50, 50, 50)));
    //Circle circlesta = circles.get(0);
    //circlesta.isstatic = true;

    // circles.add(new Circle(new PVector(130, 200), new PVector(-2.5f, -1.8f), 70, color(100, 100, 100)));

    //circles.add(new Circle(new PVector(190, 400), new PVector(1.5f, 1f), 70, color(100, 100, 100), circles.get(0))); 

    //circles.add(new Circle(new PVector(300, 300), new PVector(0, 0), 300, color(0, 0, 0)));
    //circles.get(0).isstatic = true;
    for (Integer i = 0; i<20; i++)
    {


      float interpolate = random(0, 1);
      PVector pos = new PVector(random(72/2, 960 - 72/2), random(72/2, 720-72/2));
      PVector speed = new PVector(random(-1, 1)*3.9f, random(-1, 1)*3.9f);
      int shrink = (int)random(0, 2);

      for (int j  = 0; j<circles.size(); j++)
      {
        Circle circle = circles.get(j);

        if (sqrt(pow(pos.x -circle.pos.x, 2) + pow(pos.y-circle.pos.y, 2)) < circle.size/2+ 72/2)
        {
          pos = new PVector(random(72/2, 960 - 72/2), random(72/2, 720-72/2));
          j = -1;
        }
      }
      while (-0.2<speed.x   && speed.x<0.2)
      {
        speed.x = random(random(-1, 1)*3.9f);
      }
      while (-0.2<speed.y   && speed.y<0.2)
      {
        speed.y = random(random(-1, 1)*1.9f);
      }
      circles.add(new Circle(pos, speed, 72, lerpColor(mincirclecolor, maxcirclecolor, interpolate)));
      Circle lastcircle = circles.get(i);
      //if(shrink == 0)
      //{
      //  lastcircle.shrink = false;
      //}
      //else
      //{
      //  lastcircle.shrink = true;
      //}
      lastcircle.posinlist = i;
    }
    // rect = new RectangleObstacle(new PVector(100, 100), 50, 150, circles);
  }
  public GameScreen()
  {
    super(color(235, 235, 235), "Game");

    Reset();
    font = loadFont("Arial-BoldMT-32.vlw");
    //levels = new Levels();
  }
  public void setTheme()
  {
    if (theme == 4)
    {
      Global.gamebackgroundcolor = color(20);
      mincirclecolor = color(255);
      maxcirclecolor = color(60);
      textcolor = color(255);
      Global.selectedcolor = color(230, 0, 0);
    }
    if (theme == 3)
    {
      Global.gamebackgroundcolor = color(0, 191, 255);
      mincirclecolor = color(100, 64, 0);
      maxcirclecolor = color(0, 128, 0);
      Global.selectedcolor = color(255, 0, 255);
      textcolor = color(0);
    }
    if (theme == 2)
    {
      Global.gamebackgroundcolor = color(190, 190, 190);
      textcolor = color(0, 0, 0);
      mincirclecolor = color(0, 0, 0);
      maxcirclecolor = color(0, 0, 255);
      Global.selectedcolor = color(255, 255, 0);
    }
    if (theme == 1)
    {
      Global.gamebackgroundcolor = color(0, 0, 0);
      textcolor = color(0, 255, 0);
      mincirclecolor = color(0, 50, 0);
      maxcirclecolor = color(0, 230, 0);
      Global.selectedcolor = color(250, 200, 0);
    }
    if (theme == 0)
    {
      Global.gamebackgroundcolor = color(235);
      textcolor = color(0);
      Global.selectedcolor = color(230, 0, 0);
      mincirclecolor = color(0);
      maxcirclecolor = color(195);
    }
    Global.mincirclecolor = mincirclecolor;
    Global.maxcirclecolor = maxcirclecolor;
    backgroundcolor = Global.gamebackgroundcolor;
  }
  public void Draw()
  {


    super.Draw();
    fill(textcolor);
    noStroke();
    textFont(font);
    text(round(Global.sum), 0, 32);
    Global.levels.alllevels.get(Global.level).Draw();
    // rect.Update();
    //rect.Draw();


    if (Global.sum < 100)
    {
      prevmillis = millis();
    }
    if (Global.sum >= 100)
    {
      millis = millis();
      Global.lastselectedcircle._color = Global.lastselectedcircle._originalcolor;
      pushMatrix();
      fill(color(0, 190, 0), alpha);
      if (alpha < 211)
      {
        alpha+=20f;
      }
      rect(0,0,width,height);
      popMatrix();
      
      if (millis - prevmillis > 1000)
      {

        Global.level++;
        prevmillis = millis();
      }
    }
    if (!Global.gameRun)
    {

      if (keyCode == ' ' && lastKeyCode != ' ')
      {
        //Reset();
        Global.levels.ResetLevel();
        alpha = 0;
      }
    }
    if (!Global.gameRun)
    {
      pushMatrix();
      if (alpha < 211)
      {
        alpha+=20;
      }

        
        
      noStroke();
      fill(Global.selectedcolor, alpha);
      rect(0, 0, width, height);
      popMatrix();
      pushMatrix();
      noFill();
      stroke(0, alpha/2 + 150);

      strokeWeight(10);
      ellipse(Global.collidedcircle1.pos.x, Global.collidedcircle1.pos.y, Global.collidedcircle1.size, Global.collidedcircle1.size);
      ellipse(Global.collidedcircle2.pos.x, Global.collidedcircle2.pos.y, Global.collidedcircle2.size, Global.collidedcircle2.size);

      popMatrix();
      
      
     
           if(alpha > 211)
     {
       
         textFont(font);
       pushMatrix();
       fill(Global.gamebackgroundcolor);
       stroke(Global.mincirclecolor);
       strokeWeight(1);
      
       rect((width*3)/4 - textWidth("Go to the main menu")/2 - 12,70-32,textWidth("Go to the main menu") + 24, 32+12);
        rect((width*3)/4 - textWidth("Go to the level selector")/2 - 12,140-32,textWidth("Go to the level selector") + 24, 32+12);
       popMatrix();
       pushMatrix();
       fill(Global.mincirclecolor);
       noStroke();
       text("Go to the main menu",(width*3)/4 - textWidth("Go to the main menu")/2,70);
       text("Go to the level selector",(width*3)/4 - textWidth("Go to the level selector")/2,140);
       popMatrix();
     }
     
     
    }
    //if (collidedcircle1 != null && !gameRun)
    //{
    //  collidedcircle1.Draw();
    //}
    //if (collidedcircle2 != null && !gameRun)
    //{
    //  collidedcircle2.Draw();
    //}
    lastKeyCode = keyCode;
  }
  public void onMouseClick()
  {
    super.onMouseClick();
    if(!Global.gameRun)
    {
      if( mouseX > (width*3)/4 - textWidth("Go to the main menu")/2 - 12 && mouseX < (width*3)/4 + textWidth("Go to the main menu")/2 +24  &&mouseY > 70-32 && mouseY < 70 + 12)
      {
        Global.manager.screentype = "MainMenu";
        alpha = 0; //<>//
        Global.levels.ResetLevel();
      }
        if( mouseX > (width*3)/4 - textWidth("Go to the level selector")/2 - 12 && mouseX < (width*3)/4 + textWidth("Go to the level selector")/2 +24  &&mouseY > 140-32 && mouseY < 140 + 12)
      {
        Global.manager.screentype = "LevelSelect";
        alpha = 0;
        Global.levels.ResetLevel();
      }
      //gameRun = true;
    }
  }
}
