public class Level
{
  ArrayList<Circle> circles;
  public boolean won = false;
  public boolean hasbeencompleted = false;
  public Level(ArrayList<Circle> circles)
  {
    this.circles = circles;
  }
  public void Draw()
  {
    Global.sum = 0;
    for (Circle circle : circles)
    {


      Global.sum += circle.number;
      circle.Draw();
      if (Global.gameRun && !won)
      {

        
        circle.Update();
        

        if (mousePressed && !circle.isstatic && Global.holdtogrow || !Global.holdtogrow)
        {
          if (sqrt(pow(mouseX - circle.pos.x, 2)+pow((mouseY -circle.pos.y), 2)) < circle.size/2)
          {

            if (round(circle.number) < 100 )
            {
             Global.lastselectedcircle = circle;
              float newsize = circle.size+6.4;
              if (circle.pos.x - (newsize/2) > 0 && circle.pos.y - (newsize/2) > 0 && circle.pos.x + (newsize/2) <width && circle.pos.y + (newsize/2) < height)
              {
                circle._color = Global.selectedcolor;
                circle.size+=6.4;
                if (circle.linkedcircle != circle)
                {
                  circle.linkedcircle._color = Global.selectedcolor;
                  circle.linkedcircle.size+=6.4;
                }
              }
              if (circle.pos.x - (newsize/2) < 0 )
              {
                circle.speed.x +=0.01f;
                // circle.speed.x = abs(circle.speed.x);
              }
              if (circle.pos.y - (newsize/2) < 0 )
              {
                circle.speed.y +=0.01f;
                //circle.speed.y = abs(circle.speed.y);
              }
              if (circle.pos.x + (newsize/2) > width  )
              {
                circle.speed.x -=0.01f;
                // circle.speed.x = -abs(circle.speed.x);
              }
              if (circle.pos.y + (newsize/2) > height )
              {
                circle.speed.y -=0.01f;
                // circle.speed.y = -abs(circle.speed.y);
              }
            }
          } else
          {
            circle._color = circle._originalcolor;
          }
        } else
        {
          if (circle.linkedcircle != circle && circle.linkedcircle._color == Global.selectedcolor)
          {
          } else
          {
            circle._color = circle._originalcolor;
          }
        }

        if (mousePressed && !circle.linkedcircle.isstatic && circle != circle.linkedcircle && Global.holdtogrow||!Global.holdtogrow &&  !circle.linkedcircle.isstatic && circle != circle.linkedcircle)
        {
          if (sqrt(pow(mouseX - circle.linkedcircle.pos.x, 2)+pow((mouseY -circle.linkedcircle.pos.y), 2)) < circle.linkedcircle.size/2)
          {

            if (circle.linkedcircle.number < 100)
            {

              float newsize = circle.linkedcircle.size+6.4;
              if (circle.linkedcircle.pos.x - (newsize/2) > 0 && circle.linkedcircle.pos.y - (newsize/2) > 0 && circle.linkedcircle.pos.x + (newsize/2) <width && circle.linkedcircle.pos.y + (newsize/2) < height)
              {
                circle.linkedcircle._color = Global.selectedcolor;
                //circle.linkedcircle.size+=3;
                if (circle.linkedcircle != circle)
                {
                  circle._color = Global.selectedcolor;
                  circle.size+=6.4;
                }
              }
              if (circle.linkedcircle.pos.x - (newsize/2) < 0)
              {
                circle.linkedcircle.speed.x +=0.01f;
              }
              if (circle.linkedcircle.pos.y - (newsize/2) < 0)
              {
                circle.linkedcircle.speed.y +=0.01f;
              }
              if (circle.linkedcircle.pos.x + (newsize/2) > 600)
              {
                circle.linkedcircle.speed.x -=0.01f;
              }
              if (circle.linkedcircle.pos.y + (newsize/2) > 600)
              {
                circle.linkedcircle.speed.y -=0.01f;
              }
            }
          } else
          {
            //circle.linkedcircle._color = circle.linkedcircle._originalcolor;
          }
        } else
        {
          //if(circle != circle.linkedcircle)
          //{
          //circle.linkedcircle._color = circle.linkedcircle._originalcolor;
          //}
        }


        for (Circle othercircle : circles)
        {
          if (othercircle != circle)
          {

            PVector newpos1 = new PVector(circle.pos.x+circle.speed.x, circle.pos.y+circle.speed.y);
            PVector newpos2 = new PVector(othercircle.pos.x+othercircle.speed.x, othercircle.pos.y+othercircle.speed.y);

            if (sqrt(pow(newpos1.x - newpos2.x, 2) + pow(newpos1.y-newpos2.y, 2)) <= circle.size/2 + othercircle.size/2)
            {


              PVector oldspeed1 = circle.speed;
              PVector oldspeed2 = othercircle.speed;


              //swap speeds method
              //circle.speed = oldspeed2;
              //othercircle.speed = oldspeed1;
            }

            if (sqrt(pow(circle.pos.x- othercircle.pos.x, 2) + pow(circle.pos.y - othercircle.pos.y, 2)) <= circle.size/2+othercircle.size/2)
            {
              if (circle._color == Global.selectedcolor||  othercircle._color == Global.selectedcolor)
              {

                Global.gameRun = false;
                circle.collided = true;
                othercircle.collided= true;
                circle._color = circle._originalcolor;
                othercircle._color = othercircle._originalcolor;
                //circle._color = Global.selectedcolor;
                //othercircle._color = Global.selectedcolor;
                Global.collidedcircle1 = circle;

                Global.collidedcircle1.posinlist = 1;
                Global.collidedcircle2 = othercircle;
                Global.collidedcircle2.posinlist = 1;
              } else
              {
                PVector oldspeed1 = circle.speed;
                PVector oldspeed2 = othercircle.speed;
                PVector d = new PVector(newpos1.x-newpos2.x, newpos1.y-newpos2.y);
                if (!circle.isstatic)
                {
                  circle.pos.add(d.normalize());
                }
                if (!othercircle.isstatic)
                {

                  othercircle.pos.sub(d.normalize());
                }
                //swap speeds method
                if (othercircle.isstatic && !circle.isstatic)
                {

                  if (circle.pos.x - circle.size/2 < othercircle.pos.x)
                  {
                    circle.speed.x = -abs(circle.speed.x);
                    //othercircle.speed.x = abs(othercircle.speed.x);
                  }
                  if (circle.pos.x + circle.size/2 > othercircle.pos.x)
                  {
                    circle.speed.x = abs(circle.speed.x);
                  }
                  if (circle.pos.y - circle.size/2 < othercircle.pos.y)
                  {
                    circle.speed.y = -abs(circle.speed.y);
                  }
                  if (circle.pos.y + circle.size/2 > othercircle.pos.y)
                  {
                    circle.speed.y = abs(circle.speed.y);
                  }
                }
                if (!circle.isstatic && !othercircle.isstatic)
                {
                  if (circle.linkedcircle != othercircle && circle.linkedcircle != circle)
                  {
                    // circle.linkedcircle.speed = oldspeed2;
                  }


                  circle.speed = oldspeed2;
                  othercircle.speed = oldspeed1;
                }


                // circle.pos.x += circle.speed.x;
                // circle.pos.y += circle.speed.y;
                //othercircle.pos.x +=othercircle.speed.x;
                // othercircle.pos.y += othercircle.speed.y;
                //if(circle.pos.x - circle.size/2 < othercircle.pos.x)
                //{
                //  circle.speed.x = -abs(circle.speed.x);
                //  //othercircle.speed.x = abs(othercircle.speed.x);
                //}
                //if(circle.pos.x + circle.size/2 > othercircle.pos.x)
                //{
                //  circle.speed.x = abs(circle.speed.x);
                //}
                //if(circle.pos.y - circle.size/2 < othercircle.pos.y)
                //{
                //  circle.speed.y = -abs(circle.speed.y);
                //}
                //if(circle.pos.y + circle.size/2 > othercircle.pos.y)
                //{
                //  circle.speed.y = abs(circle.speed.y);
                //}
              }
            }
          }
        }
      }
    }
    if(Global.sum >=100)
    {
      won = true;
      hasbeencompleted = true;
    }
    else
    {
      won = false;
    }
  }
  void Reset()
  {
  }
}
