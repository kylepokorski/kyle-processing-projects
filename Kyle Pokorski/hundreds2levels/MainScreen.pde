public class MainMenuScreen extends Screen
{
  PFont font;
  public MainMenuScreen()
  {
    super(Global.gamebackgroundcolor,"MainMenu");
     font = loadFont("Arial-BoldMT-32.vlw");
     Global.levels = new Levels();
    
  }
  public void Draw()
  {
    super.Draw();
    fill(Global.mincirclecolor);
    textFont(font);
    text("Hundreds",width/2 - textWidth("Hundreds")/2,30);
    text("Play Game",width/2 - textWidth("Play Game")/2,190);
  }
  public void onMouseClick()
  {
    super.onMouseClick();
    if(mouseX > width/2 - textWidth("Play Game")/2 && mouseX < width/2 + textWidth("Play Game") && mouseY > 160 && mouseY < 190)
    {
      Global.manager.screentype = "LevelSelect";
    }
  }
}
