public static class Global
{
  public static color selectedcolor;
  public static color gamebackgroundcolor;
  public static int level = 0;
  public static boolean gameRun = true;
  public static float sum = 0;
  public static Circle collidedcircle1;
  public static Circle collidedcircle2;
  public static boolean holdtogrow = true;
  public static Circle lastselectedcircle;
  public static color mincirclecolor;
  public static color maxcirclecolor;
  public static Levels levels;
  public static ScreenManager manager;
}
