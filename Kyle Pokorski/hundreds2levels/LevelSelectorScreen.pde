public class LevelSelectorScreen extends Screen
{
  //Levels levels
  PFont font;
  PFont fontsmall;
  int rows;
  public LevelSelectorScreen()
  {
    super(Global.gamebackgroundcolor, "LevelSelect");
    font = loadFont("Arial-BoldMT-32.vlw");
    fontsmall = loadFont("Arial-BoldMT-16.vlw");
  }
  public void Draw()
  {
    rows = ceil(Global.levels.alllevels.size()/10f);
    super.Draw();
    textFont(font);
    noStroke();
    fill(0, 0, 0);
   
    for (int i = 0; i< rows; i++)
    {
      for (int j = 0; j< 10; j++)
      {
        if (j + i*10 < Global.levels.alllevels.size())
        {

          // textFont(fi);
          if(Global.levels.alllevels.get(j+i*10).hasbeencompleted)
          {
            fill(Global.selectedcolor);
          }
          else
         {
           fill(Global.mincirclecolor);
         }
          text(j+ i*10 + 1, 32 + 60*j, 130 + 50*i);
        }
      }
    }
  }
  public void onMouseClick()
  {
    for(int i = 0; i< rows; i++)
    {
       for (int j = 0; j< 10; j++)
      {
        if (j + i*10 < Global.levels.alllevels.size())
        {
int val = j + i*10;
String s = "" + val;
          if(mouseX > 32 + 60* j && mouseX < 32 + 60*j + textWidth(s))
          {
            Global.level = val;
            Global.manager.screentype = "Game";
          }
          // textFont(fi);
          //text(j+ i*10 + 1, 32 + 64*j, 130 + 64*i);
        }
      }
    }
  }
}
