public class Levels
{
  public ArrayList<Level> alllevels = new ArrayList<Level>();
  Level level1;
  Level level2;
  Level level3;
  Level level4;
  ArrayList<Circle> level1obstacles = new ArrayList<Circle>();
  ArrayList<Circle> level2obstacles = new ArrayList<Circle>();
  ArrayList<Circle> level3obstacles = new ArrayList<Circle>();
  ArrayList<Circle> level4obstacles = new ArrayList<Circle>();
void setLevels()
{
  level1obstacles.clear();
  level2obstacles.clear();
  level3obstacles.clear();
  level4obstacles.clear();
   level1obstacles.add(new Circle(new PVector(width/2, height/2), new PVector(0, 0), 72, Global.mincirclecolor));
   
   
     level2obstacles.add(new Circle(new PVector(width/2, height/2 - height/5), new PVector(0, 0), 72, Global.mincirclecolor));
    level2obstacles.add(new Circle(new PVector(width/2- width/5, height/2 + height/5), new PVector(0, 0), 72, Global.mincirclecolor));
    level2obstacles.add(new Circle(new PVector(width/2+ width/5, height/2 + height/5), new PVector(0, 0), 72, Global.mincirclecolor));

    level2obstacles.get(1).posinlist = 1;
    level2obstacles.get(2).posinlist = 2;
    
     for (Integer i = 0; i<5; i++)
    {


      float interpolate = random(0, 1);
      PVector pos = new PVector(random(72/2, 960 - 72/2), random(72/2, 720-72/2));
      PVector speed = new PVector(random(-1, 1)*3.9f, random(-1, 1)*3.9f);
      int shrink = (int)random(0, 2);

      for (int j  = 0; j<level3obstacles.size(); j++)
      {
        Circle circle = level3obstacles.get(j);

        if (sqrt(pow(pos.x -circle.pos.x, 2) + pow(pos.y-circle.pos.y, 2)) < circle.size/2+ 72/2)
        {
          pos = new PVector(random(72/2, 960 - 72/2), random(72/2, 720-72/2));
          j = -1;
        }
      }
      while (-0.2<speed.x   && speed.x<0.2)
      {
        speed.x = random(random(-1, 1)*3.9f);
      }
      while (-0.2<speed.y   && speed.y<0.2)
      {
        speed.y = random(random(-1, 1)*3.9f);
      }
      level3obstacles.add(new Circle(pos, speed, 72, lerpColor(Global.mincirclecolor, Global.maxcirclecolor, interpolate)));
      Circle lastcircle = level3obstacles.get(i);
      //if(shrink == 0)
      //{
      //  lastcircle.shrink = false;
      //}
      //else
      //{
      //  lastcircle.shrink = true;
      //}
      lastcircle.posinlist = i;
    }
    for (Integer i = 0; i<10; i++)
    {


      float interpolate = random(0, 1);
      PVector pos = new PVector(random(72/2, 960 - 72/2), random(72/2, 720-72/2));
      PVector speed = new PVector(random(-1, 1)*4.2f, random(-1, 1)*4.2f);
      

      for (int j  = 0; j<level4obstacles.size(); j++)
      {
        Circle circle = level4obstacles.get(j);

        if (sqrt(pow(pos.x -circle.pos.x, 2) + pow(pos.y-circle.pos.y, 2)) < circle.size/2+ 72/2)
        {
          pos = new PVector(random(72/2, 960 - 72/2), random(72/2, 720-72/2));
          j = -1;
        }
      }
      while (-0.2<speed.x   && speed.x<0.2)
      {
        speed.x = random(random(-1, 1)*4.2f);
      }
      while (-0.2<speed.y   && speed.y<0.2)
      {
        speed.y = random(random(-1, 1)*4.2f);
      }
      level4obstacles.add(new Circle(pos, speed, 72, lerpColor(Global.mincirclecolor, Global.maxcirclecolor, interpolate)));
      Circle lastcircle = level4obstacles.get(i);
      //if(shrink == 0)
      //{
      //  lastcircle.shrink = false;
      //}
      //else
      //{
      //  lastcircle.shrink = true;
      //}
      lastcircle.posinlist = i;
    }
}
  public Levels()
  {
    setLevels();
   
    level1 = new Level(level1obstacles);
    level2 = new Level(level2obstacles);
    level3 = new Level(level3obstacles);
    level4 = new Level(level4obstacles);

    alllevels.add(level1);
    alllevels.add(level2);
    alllevels.add(level3);
    alllevels.add(level4);
  }
  public void ResetLevel()
  {
    Global.gameRun = true;
    setLevels();
  }
}
