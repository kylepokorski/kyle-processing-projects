float xposition = 0;
float rotations = 0;
float rotationchange = 0.01f;
int spokes= 8;
void setup()
{
  size(600, 600);
}
void generateSpokes(int offset)
{
  stroke(150, 150, 150);
  strokeWeight(5);
  pushMatrix();
  translate(xposition+ offset, 205);
  rotate(rotations);
  for (int i = 0; i<spokes; i++)
  {
    line(-17, -17, 17, 17);
    rotate(PI/4);
  }

  popMatrix();
}
void draw()
{
  rotations+=rotationchange;
  if (xposition < 600)
  {
    xposition ++;
  } else
  {
    xposition = -200;
  }

  background(0, 191, 255);
  fill(0, 0, 128);
  stroke(0, 0, 0);
  strokeWeight(1);
  rect(xposition + 50, 60, 100, 50);
  rect(xposition, 100, 200, 90);
  fill(0, 0, 0);
  noStroke();

  ellipse(xposition + 25, 205, 50, 50);
  ellipse(xposition + 175, 205, 50, 50);
  fill(150, 150, 150);
  ellipse(xposition+25, 205, 25, 25);
  ellipse(xposition+175, 205, 25, 25);
  generateSpokes(25);
  generateSpokes(175);
}
